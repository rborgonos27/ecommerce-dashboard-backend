const { isValidPassword, isValidEmail, isEmptyObject } = require('../../utils/helper');

describe('Helper test valid password', () => {
  it('Test is valid password', () => {
     const text = "P@ssw0rd";
     const isValid = isValidPassword(text);
     expect(isValid).toBeTruthy();
  });
  it('Test is valid password', () => {
      const text = "_P0ssw0rd";
      const isValid = isValidPassword(text);
      expect(isValid).toBeTruthy();
  });
  it('Test is invalid password', () => {
      const text = "password";
      const isValid = isValidPassword(text);
      expect(isValid).toBeFalsy();
  });
  it('Test is invalid password less than 8 char', () => {
      const text = "passwo";
      const isValid = isValidPassword(text);
      expect(isValid).toBeFalsy();
  });
  it('Test is invalid password no special char', () => {
      const text = "password0A";
      const isValid = isValidPassword(text);
      expect(isValid).toBeFalsy();
  });
    it('Test is invalid password no capital case', () => {
        const text = "password0!";
        const isValid = isValidPassword(text);
        expect(isValid).toBeFalsy();
    });
    it('Test is invalid password empty password', () => {
        const text = "";
        const isValid = isValidPassword(text);
        expect(isValid).toBeFalsy();
    });
});

describe('Helper test valid email', () => {
    it('Test is valid email', () => {
        const email = "test@email.com";
        const isValid = isValidEmail(email);
        expect(isValid).toBeTruthy();
    });
    it('Test is valid email with two dot', () => {
        const email = "test.me@email.com";
        const isValid = isValidEmail(email);
        expect(isValid).toBeTruthy();
    });
    it('Test is invalid email', () => {
        const email = "email";
        const isValid = isValidEmail(email);
        expect(isValid).toBeFalsy();
    });
    it('Test is invalid email', () => {
        const email = "email.com@";
        const isValid = isValidEmail(email);
        expect(isValid).toBeFalsy();
    });
    it('Test is invalid email empty text', () => {
        const email = "";
        const isValid = isValidEmail(email);
        expect(isValid).toBeFalsy();
    });
    it('Test is valid email double dot', () => {
        const email = "test.test.me@gmai.com.au";
        const isValid = isValidEmail(email);
        expect(isValid).toBeTruthy();
    });
    it('Test is invalid email double dot', () => {
        const email = "test.test.me";
        const isValid = isValidEmail(email);
        expect(isValid).toBeFalsy();
    });
});

describe('Helper test is empty object', () => {
    it('Test if object is empty, return true', () => {
       const object = {};
       expect(isEmptyObject(object)).toBeTruthy();
    });
    it('Test if object is not empty, return false', () => {
        const object = { "key": "value" };
        expect(isEmptyObject(object)).toBeFalsy();
    });
});



