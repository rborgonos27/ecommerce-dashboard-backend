const appUser = require('../../utils/app_user');
const User = require('../../models/users');
const mongoose = require('mongoose');
const faker = require('faker');
const { MongoMemoryServer } = require('mongodb-memory-server');
jasmine.DEFAULT_TIMEOUT_INTERVAL = 120000;

describe('Config App User', () => {
    const user = {
        Email: faker.internet.email(),
        FirstName: faker.name.firstName(),
        LastName: faker.name.lastName(),
        Password: faker.internet.password(),
        Role: 'SuperAdmin',
        Status: 'Active',
        DateCreated: new Date(),
        DateUpdated: new Date()
    };
    beforeAll(async () => {
        jest.setTimeout(120 * 1000);
        mongod = new MongoMemoryServer({
            instance: {
                dbName: 'jest',
            },
            binary: {
                version: '4.0.2',
            },
            debug: true,
        });

        const mongoUri = await mongod.getConnectionString();
        console.log(mongoUri);
        await mongoose.connect(
            mongoUri,
            { useNewUrlParser: true }
        );
        mongoose.set('debug', true);
    });
    afterAll(async (done) => {
        mongoose.connection.close();
        mongod.stop();
        done();
    });
    beforeEach(async () => {
        console.log('before');
        await User.model(user).save();
    });
    afterEach(async () => {
        console.log('after');
        await User.model.deleteMany({});
    });
    it('Test app user get valid user success', async () => {
        const retrieveUser = await appUser.getUser(user.Email);
        expect(retrieveUser.FirstName).toBe(user.FirstName);
        expect(retrieveUser.LastName).toBe(user.LastName);
        expect(retrieveUser.Email).toBe(user.Email);
    });
    it('Test app user get invalid user', async () => {
        try {
            await appUser.getUser('testerrorrr@test.com');
        } catch (e) {
            console.log(e.message);
            expect('User not found.').toBe(e.message);
        }
    });
});