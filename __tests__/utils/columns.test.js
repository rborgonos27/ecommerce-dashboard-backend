const columns = require('../../utils/columns')
const GraphQLDate = require('graphql-date');
const { GraphQLID, GraphQLString } = require('graphql');


const design = {
    label: 'User',
    cols: {
        'Email': {type: String},
        'FirstName': {type: String},
        'LastName': {type: String},
        'Password': {type: String},
        'Role': {type: String},
        'DateCreated': { type: Date, graphQL: GraphQLDate },
        'DateUpdated': { type: Date, graphQL: GraphQLDate }
    }
};

describe('Utils GenerateGraphQlSchema columns helper', () => {
    it('GenerateGraphQlSchema test without value suffix', () => {
        const graphqlSchema = columns.GenerateGraphQlSchema(design.cols);
        const expected = {
            id: { type: GraphQLID, resolve: ({ _id }) => _id.toString() },
            Email: { type: GraphQLString },
            FirstName: { type: GraphQLString },
            LastName: { type: GraphQLString },
            Password: { type: GraphQLString },
            Role: { type: GraphQLString },
            DateCreated: { type: GraphQLDate },
            DateUpdated: { type: GraphQLDate }
        };
        expect(graphqlSchema.id.resolve({ _id: 'id' })).toBeDefined();
        expect(graphqlSchema.toString()).toStrictEqual(expected.toString());
    });
    it('GenerateGraphQlSchema test without value suffix', () => {
        const graphqlSchema = columns.GenerateGraphQlSchema(design.cols, true);
        const expected = {
            id: { type: GraphQLID, resolve: ({ _id }) => _id.toString() },
            Email: { type: GraphQLString },
            Email_val: { type: GraphQLString },
            FirstName: { type: GraphQLString },
            FirstName_val: { type: GraphQLString },
            LastName: { type: GraphQLString },
            LastName_val: { type: GraphQLString },
            Password: { type: GraphQLString },
            Password_val: { type: GraphQLString },
            Role: { type: GraphQLString },
            Role_val: { type: GraphQLString },
            DateCreated: { type: GraphQLDate },
            DateCreated_val: { type: GraphQLDate },
            DateUpdated: { type: GraphQLDate },
            DateUpdated_val: { type: GraphQLDate }
        };
        expect(graphqlSchema.id.resolve({ _id: 'id' })).toBeDefined();
        expect(graphqlSchema.toString()).toStrictEqual(expected.toString());
    });
    it('GenerateGraphQlSchema test exclude column', () => {
        const graphqlSchema = columns.GenerateGraphQlSchema(design.cols, false, ['Password']);
        const expected = {
            id: { type: GraphQLID, resolve: ({ _id }) => _id.toString() },
            Email: { type: GraphQLString },
            FirstName: { type: GraphQLString },
            LastName: { type: GraphQLString },
            Role: { type: GraphQLString },
            DateCreated: { type: GraphQLDate },
            DateUpdated: { type: GraphQLDate }
        };
        expect(graphqlSchema.id.resolve({ _id: 'id' })).toBeDefined();
        expect(graphqlSchema.toString()).toStrictEqual(expected.toString());
    });
});

describe('Utils GenerateSchema columns helper', () => {
    it('Test generate schema for mongoose', () => {
        const schema = columns.GenerateSchema(design.cols);
        const expected = {
            Email: String,
            FirstName: String,
            LastName: String,
            Password: String,
            Role: String,
            DateCreated: Date,
            DateUpdated: Date,
        };
        expect(schema).toStrictEqual(expected);
    });
    it('Test generate schema for mongoose with value suffix', () => {
        const schema = columns.GenerateSchema(design.cols, true);
        const expected = {
            Email: String,
            Email_val: String,
            FirstName: String,
            FirstName_val: String,
            LastName: String,
            LastName_val: String,
            Password: String,
            Password_val: String,
            Role: String,
            Role_val: String,
            DateCreated: Date,
            DateCreated_val: Date,
            DateUpdated: Date,
            DateUpdated_val: Date
        };
        expect(schema).toStrictEqual(expected);
    });
});