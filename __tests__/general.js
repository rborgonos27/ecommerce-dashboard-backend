const MongoMemoryServer = require('mongodb-memory-server').MongoMemoryServer;
const mongoose = require('mongoose');

let server;

exports.beforeEach = () => {
  server = new MongoMemoryServer();
  return server.getUri().then(uri => {
    mongoose.connect(uri, {
      dbName: 'jest-test',
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false
    });
  });
};

exports.afterEach = () => {
  return mongoose.disconnect().then(() => {
    return server.stop();
  });
};

describe("Setup Jest", () => {
  test("should run jest", () => {
    expect(true).toEqual(true);
  });
});
