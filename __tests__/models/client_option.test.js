const faker = require('faker');
const ClientOption = require('../../models/client_option');
const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
const moment = require('moment');
jasmine.DEFAULT_TIMEOUT_INTERVAL = 120000;

const createClientOption = (data) => {
    return new ClientOption.model(data).save();
};
let mongod;
describe('Test Client Option Model', () => {
    beforeAll(async () => {
        jest.setTimeout(120 * 1000);
        mongod = new MongoMemoryServer({
            instance: {
                dbName: 'jest',
            },
            binary: {
                version: '4.0.2',
            },
            debug: true,
        });

        const mongoUri = await mongod.getConnectionString();
        console.log(mongoUri);
        await mongoose.connect(
            mongoUri,
            { useNewUrlParser: true }
        );
        mongoose.set('debug', true);
    });
    afterAll(async (done) => {
        mongoose.connection.close();
        mongod.stop();
        done();
    });
    it('Test Client Option model', (done) => {
        const fakeClientId = mongoose.Types.ObjectId();
        const clientOptionData = {
            ClientId: fakeClientId,
            CanSync: true,
            DateLastRecord: moment(),
            DateLastSynced: moment(),
            DateCreated: moment(),
            DateUpdated: moment(),
        };
        createClientOption(clientOptionData).then(data => {
            expect(data._id).toBeDefined();
            expect(data.ClientId.toString()).toBe(clientOptionData.ClientId.toString());
            expect(data.CanSync).toBe(clientOptionData.CanSync);
            expect(data.DateLastSynced.getDate()).toBe(clientOptionData.DateLastSynced.toDate().getDate());
            expect(data.DateLastRecord.getDate()).toBe(clientOptionData.DateLastRecord.toDate().getDate());
            done();
        });
    });
    it('Verify client option attributes', () => {
        expect(ClientOption).toHaveProperty('design');
        expect(ClientOption).toHaveProperty('model');
        expect(ClientOption).toHaveProperty('schema');
        expect(ClientOption).toHaveProperty('type');
    });
    it('Verify client option design', () => {
        expect(ClientOption.design).toBeDefined();
        expect(ClientOption.design).toHaveProperty('label');
        expect(ClientOption.design).toHaveProperty('cols');
    });
});