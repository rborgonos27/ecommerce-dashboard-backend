const faker = require('faker');
const Address = require('../../models/addresses');
const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
jasmine.DEFAULT_TIMEOUT_INTERVAL = 120000;

const createAddress = (data) => {
    return new Address.model(data).save();
};
let mongod;
describe('Test Address model', () => {
    beforeAll(async () => {
        jest.setTimeout(120 * 1000);
        mongod = new MongoMemoryServer({
            instance: {
                dbName: 'jest',
            },
            binary: {
                version: '4.0.2',
            },
            debug: true,
        });

        const mongoUri = await mongod.getConnectionString();
        console.log(mongoUri);
        await mongoose.connect(
            mongoUri,
            { useNewUrlParser: true }
        );
        mongoose.set('debug', true);
    });
    afterAll(async (done) => {
        mongoose.connection.close();
        mongod.stop();
        done();
    });
    it('Test Address model', (done) => {
        const addressData = {
            Nickname: faker.name.firstName('m'),
            ShippingName: faker.name.firstName('m'),
            Address1: faker.address.streetAddress(),
            Address2: faker.address.streetAddress(),
            City: faker.address.city(),
            State: faker.address.state(),
            PostalCode: faker.address.zipCode(),
            PhoneNumber: faker.phone.phoneNumber(),
            DateCreated: Date(),
            DateUpdated: Date(),
        };
        createAddress(addressData).then(data => {
           expect(data._id).toBeDefined();
           expect(data.Nickname).toBe(addressData.Nickname);
           expect(data.ShippingName).toBe(addressData.ShippingName);
           expect(data.Address1).toBe(addressData.Address1);
           expect(data.Address2).toBe(addressData.Address2);
           expect(data.City).toBe(addressData.City);
           expect(data.State).toBe(addressData.State);
           expect(data.PostalCode).toBe(addressData.PostalCode);
           expect(data.PhoneNumber).toBe(addressData.PhoneNumber);
           done();
        });
    });
    it('Verify address attributes', () => {
        expect(Address).toHaveProperty('design');
        expect(Address).toHaveProperty('model');
        expect(Address).toHaveProperty('schema');
        expect(Address).toHaveProperty('type');
    });
    it('Verify address design', () => {
        expect(Address.design).toBeDefined();
        expect(Address.design).toHaveProperty('label');
        expect(Address.design).toHaveProperty('cols');
    });
});