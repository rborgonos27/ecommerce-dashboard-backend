const faker = require('faker');
const Client = require('../../models/clients');
const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
jasmine.DEFAULT_TIMEOUT_INTERVAL = 120000;

const createClient = (data) => {
    return new Client.model(data).save();
};
let mongod;
describe('Test User Resolver query', () => {
    beforeAll(async () => {
        jest.setTimeout(120 * 1000);
        mongod = new MongoMemoryServer({
            instance: {
                dbName: 'jest',
            },
            binary: {
                version: '4.0.2',
            },
            debug: true,
        });

        const mongoUri = await mongod.getConnectionString();
        console.log(mongoUri);
        await mongoose.connect(
            mongoUri,
            { useNewUrlParser: true }
        );
        mongoose.set('debug', true);
    });
    afterAll(async (done) => {
        mongoose.connection.close();
        mongod.stop();
        done();
    });
    it('Test Client model', (done) => {
        const fakeUserId = mongoose.Types.ObjectId();
        const fakeAddressId = mongoose.Types.ObjectId();
        const clientData = {
            Name: faker.name.firstName('m'),
            UserIds: [fakeUserId],
            MarketPlace: {
                CountryCode: "PH",
                EndPoint: faker.internet.url(),
                MarketPlaceId: faker.lorem.word(),
            },
            SellerId: faker.lorem.word(),
            MwsAuthToken: faker.lorem.word(),
            AddressIds: [fakeAddressId],
            DateCreated: new Date(),
            DateUpdated: new Date()
        };
        createClient(clientData).then(data => {
            expect(data._id).toBeDefined();
            expect(data.Name).toBe(clientData.Name);
            expect(data.SellerId).toBe(clientData.SellerId);
            expect(data.MarketPlace[0].CountryCode).toBe(clientData.MarketPlace.CountryCode);
            expect(data.MarketPlace[0].EndPoint).toBe(clientData.MarketPlace.EndPoint);
            expect(data.MarketPlace[0].MarketPlaceId).toBe(clientData.MarketPlace.MarketPlaceId);
            expect(data.MwsAuthToken).toBe(clientData.MwsAuthToken);
            done();
        });
    });
    it('Verify client attributes', () => {
        expect(Client).toHaveProperty('design');
        expect(Client).toHaveProperty('model');
        expect(Client).toHaveProperty('schema');
        expect(Client).toHaveProperty('type');
    });
    it('Verify client design', () => {
        expect(Client.design).toBeDefined();
        // expect(Client.design).toHaveProperty('label');
        expect(Client.design).toHaveProperty('cols');
    });
});