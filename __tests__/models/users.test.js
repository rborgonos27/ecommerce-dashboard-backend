const faker = require('faker');
const users = require('../../models/users');
const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
jasmine.DEFAULT_TIMEOUT_INTERVAL = 120000;

const createUser = (data) => {
    return new users.model(data).save();
};
let mongod;
describe('Models Users', () => {
    beforeAll(async () => {
        jest.setTimeout(120 * 1000);
        mongod = new MongoMemoryServer({
            instance: {
                dbName: 'jest',
            },
            binary: {
                version: '4.0.2',
            },
            debug: true,
        });

        const mongoUri = await mongod.getConnectionString();
        console.log(mongoUri);
        await mongoose.connect(
            mongoUri,
            { useNewUrlParser: true }
        );
        mongoose.set('debug', true);
    });
    afterAll(async (done) => {
        mongoose.connection.close();
        mongod.stop();
        done();
    });
    it('Testing User model', (done) => {
        const userData = {
            Email: faker.internet.email(),
            FirstName: faker.name.firstName('m'),
            LastName: faker.name.lastName('m'),
            Password: faker.internet.password(),
            Role: 'Admin',
            Status: 'Active',
            DateCreated: new Date(),
            DateUpdated: new Date()
        };
        createUser(userData).then(data => {
            expect(data._id).toBeDefined();
            expect(data.Email).toBe(userData.Email);
            expect(data.FirstName).toBe(userData.FirstName);
            expect(data._id).toBeDefined();
            done();
        });
    });
    it('Verify model attributes', () => {
       expect(users).toHaveProperty('design');
       expect(users).toHaveProperty('model');
       expect(users).toHaveProperty('schema');
       expect(users).toHaveProperty('type');
   });
   it('Verify User Design', () => {
      expect(users.design).toBeDefined();
      expect(users.design).toHaveProperty('label');
      expect(users.design).toHaveProperty('cols');
   });
});