const faker = require('faker');
const users = require('../../../models/users');
const clients = require('../../../models/clients');
const ClientResolver = require('../../../schema/resolver/client');
const mongoose = require('mongoose');
const { beforeEach, afterEach } = require('../../general');

const createUser = (data) => {
    return new users.model(data).save();
};
const userData = {
    Email: faker.internet.email(),
    FirstName: faker.name.firstName('m'),
    LastName: faker.name.lastName('m'),
    Password: faker.internet.password(),
    Role: 'Admin',
    Status: 'Active',
    DateCreated: new Date(),
    DateUpdated: new Date()
};
const addressData = {
    Nickname: faker.name.firstName('m'),
    ShippingName: faker.name.firstName('m'),
    Address1: faker.address.streetAddress(),
    Address2: faker.address.streetAddress(),
    City: faker.address.city(),
    State: faker.address.state(),
    PostalCode: faker.address.zipCode(),
    PhoneNumber: faker.phone.phoneNumber(),
    DateCreated: Date(),
    DateUpdated: Date(),
};
const clientData = {
    Name: faker.name.firstName('m'),
    MarketPlace: {
        CountryCode: "PH",
        EndPoint: faker.internet.url(),
        MarketPlaceId: faker.lorem.word(),
    },
    SellerId: faker.lorem.word(),
    MwsAuthToken: faker.lorem.word(),
    DateCreated: new Date(),
    DateUpdated: new Date()
};
describe('Client Resolver tests', () => {
    beforeAll(done => {
        jest.setTimeout(120000);
        beforeEach().then(() => done())
    });
    afterAll(done => afterEach().then(() => done()));
    it('Test Add Client Resolver', (done) => {
        createUser(userData).then(data => {
            clientData.UserIds = [data._id];
            clientData.Address = addressData;
            clientData.Name = faker.name.firstName('m');
            const args = clientData;
            ClientResolver.clientSetup(null, args).then(data => {
                expect(data._id).toBeDefined();
                expect(data.SellerId).toBe(args.SellerId);
                expect(data.MwsAuthToken).toBe(args.MwsAuthToken);
                done();
            });
        });
    });
    it('Test Update Client Resolver', (done) => {
        clients.model(clientData).save().then(data => {
            const id = data._id;
            const fakeUserId = mongoose.Types.ObjectId();
            const args = {
                id,
                Name: 'Updated Data',
                MarketPlace: clientData.MarketPlace,
                UserIds: [fakeUserId],
                Address: addressData
            };
            ClientResolver.updateClient(null, args).then(clientResult => {
                expect(clientResult._id.toString()).toBe(args.id.toString());
                expect(clientResult.Name).toBe(args.Name);
                done();
            });
        });
    });
    it('Test Update Client Resolver Complete setup', (done) => {
        clients.model(clientData).save().then(data => {
            const id = data._id;
            const fakeUserId = mongoose.Types.ObjectId();
            const args = {
                id,
                Name: 'Updated Data',
                MarketPlace: clientData.MarketPlace,
                UserIds: [fakeUserId],
                Address: addressData,
                SellerId: faker.lorem.word(),
                MwsAuthToken: faker.lorem.word()
            };
            ClientResolver.updateClient(null, args).then(clientResult => {
                expect(clientResult._id.toString()).toBe(args.id.toString());
                expect(clientResult.Name).toBe(args.Name);
                expect(clientResult.IsSetupComplete).toBeTruthy();
                done();
            });
        });
    });
    it('Test Add Client Resolver, Client Already Exists', async () => {
        const data = await createUser(userData);
        clientData.UserIds = [data._id];
        clientData.Address = addressData;
        clientData.Name = faker.name.firstName('m');
        const args = clientData;
        const clientResult = await ClientResolver.clientSetup(null, args);
        expect(clientResult._id).toBeDefined();
        expect(clientResult.SellerId).toBe(args.SellerId);
        expect(clientResult.MwsAuthToken).toBe(args.MwsAuthToken);

        try {
            await ClientResolver.clientSetup(null, args);
        } catch (error) {
            expect(error.message).toBe('Client name already exists.');
        }
    });
    it('Test Update/Delete Client Resolver, Client Not Found', async () => {
        const data = await createUser(userData);
        const fakeClientId = mongoose.Types.ObjectId();
        clientData.UserIds = [data._id];
        clientData.Address = addressData;
        clientData.Name = faker.name.firstName('m');
        clientData.id = fakeClientId;
        const args = clientData;
        try {
            await ClientResolver.updateClient(null, args);
        } catch (error) {
            expect(error.message).toBe('Client not found.');
        }

        try {
            await ClientResolver.deleteClient(null, { id: fakeClientId });
        } catch (error) {
            expect(error.message).toBe('Client not found.');
        }
    });
    it('Test Delete Client Resolver, Success', async () => {
        const data = await createUser(userData);
        clientData.UserIds = [data._id];
        clientData.Address = addressData;
        clientData.Name = faker.name.firstName('m');
        let args = clientData;
        const clientResult = await ClientResolver.clientSetup(null, args);
        expect(clientResult._id).toBeDefined();
        expect(clientResult.SellerId).toBe(args.SellerId);
        expect(clientResult.MwsAuthToken).toBe(args.MwsAuthToken);
        args = {
            id: clientResult._id
        };
        const deleteResult = await ClientResolver.deleteClient(null, args);
        expect(deleteResult.Success).toBeTruthy();
        expect(deleteResult.Message).toBe('Successfully deleted.');
    });
    it('Test Delete/Update Client Resolver, Client Already Deleted', async () => {
        const data = await createUser(userData);
        clientData.UserIds = [data._id];
        clientData.Address = addressData;
        clientData.Name = faker.name.firstName('m');
        let args = clientData;
        const clientResult = await ClientResolver.clientSetup(null, args);
        expect(clientResult._id).toBeDefined();
        expect(clientResult.SellerId).toBe(args.SellerId);
        expect(clientResult.MwsAuthToken).toBe(args.MwsAuthToken);
        args = {
            id: clientResult._id
        };
        const deleteResult = await ClientResolver.deleteClient(null, args);
        expect(deleteResult.Success).toBeTruthy();
        expect(deleteResult.Message).toBe('Successfully deleted.');
        try {
            await ClientResolver.deleteClient(null, args);
        } catch (error) {
            expect(error.message).toBe('Client is already deleted.');
        }
        try {
            args.UserIds = [data._id];
            args.Address = addressData;
            args.Name = faker.name.firstName('m');
            await ClientResolver.updateClient(null, args);
        } catch (error) {
            expect(error.message).toBe('Client is already deleted.');
        }
    });
});