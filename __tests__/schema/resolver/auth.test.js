const faker = require('faker');
const UserResolver = require('../../../schema/resolver/user');
const User = require('../../../models/users');
const AuthResolver = require('../../../schema/resolver/auth');
const forgotPassword = require('../../../models/forgot_password');
const { beforeEach, afterEach } = require('../../general');
const moment = require('moment');

const userData = {
    Email: faker.internet.email(),
    FirstName: faker.name.firstName('m'),
    LastName: faker.name.lastName('m'),
    Password: faker.internet.password(10, false, null, '0@4Fp##P'),
    Role: 'Admin',
    Status: 'Active',
    DateCreated: new Date(),
    DateUpdated: new Date()
};
describe('Test Auth Resolver', () => {
    beforeAll(done => {
        jest.setTimeout(120000);
        process.env.JWT_SECRET=faker.name.firstName('m');
        beforeEach().then(() => done())
    });
    afterAll(done => afterEach().then(() => done()));
   it('Test Login login valid user, return success', (done) => {
       userData.ConfirmPassword = userData.Password;
       let args = userData;
       const loginArgs = {
          Email: userData.Email,
          Password: userData.Password,
       };
       UserResolver.addUser(null, args).then((user) => {
           expect(user._id).toBeDefined();
           expect(user.Email).toBe(userData.Email.toLowerCase());
           args = loginArgs;
           AuthResolver.login(null, args).then((data) => {
               expect(data.Email).toBe(userData.Email.toLowerCase());
               expect(data.Password === loginArgs.Password).toBeFalsy();
               done();
           });
       });
   });
    it('Test Login login valid user, but invalid credential', async () => {
        userData.ConfirmPassword = userData.Password;
        userData.Email = faker.internet.email();
        let args = userData;
        const loginArgs = {
            Email: userData.Email,
            Password: userData.Password,
        };
        const addedUser = await UserResolver.addUser(null, args);
        expect(addedUser._id).toBeDefined();
        expect(addedUser.Email).toBe(userData.Email.toLowerCase());
        loginArgs.Password = faker.internet.password(10, false, null, '0@4Fp##P');
        args = loginArgs;
        try {
            await AuthResolver.login(null, args);
        } catch (error) {
            expect(error.message).toBe('Login failed. Invalid credential.');
        }
    });
   it('Test Login login invalid user, return error message', async () => {
       const args = {
           Email: faker.internet.email(),
           Password: faker.internet.password(10, false, null, '0@4Fp##P'),
       };
       try {
           await AuthResolver.login(null, args);
       } catch (error) {
           expect(error.message).toBe('Authentication failed. User not found.');
       }
   });
   it('Test Forgot Password with valid email, return success', () => {

   });
   it('Test Forgot Password, Key cannot be blank', async () => {
       let args = {
           Password: '',
           ConfirmPassword: '',
       };
       try {
           await AuthResolver.resetPassword(null, args);
       } catch (error) {
           expect(error.message).toBe('Reset password key can\'t be blank');
       }
   });
    it('Test Forgot Password, New password can\'t be blank', async () => {
        let args = {
            Key: 'key',
            Password: '',
            ConfirmPassword: '',
        };
        try {
            await AuthResolver.resetPassword(null, args);
        } catch (error) {
            expect(error.message).toBe('New password can\'t be blank');
        }
    });
    it('Test Forgot Password, Reset password key not found', async () => {
        const textInput = faker.internet.password(10, false, null, '0@4Fp##P');
        let args = {
            Key: 'key',
            Password: textInput,
            ConfirmPassword: textInput,
        };
        try {
            await AuthResolver.resetPassword(null, args);
        } catch (error) {
            expect(error.message).toBe('Reset password key not found');
        }
    });
    it('Test Forgot Password, Key is expired', async () => {
        const fwData = await forgotPassword.model({
            Key: 'Key',
            Email: faker.internet.email().toLowerCase(),
            DateCreated: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
            DateExpiration: moment().utc().subtract(1, 'day').format('YYYY-MM-DD HH:mm:ss')
        }).save();
        const textInput = faker.internet.password(10, false, null, '0@4Fp##P');
        let args = {
            Key: fwData.Key,
            Password: textInput,
            ConfirmPassword: textInput,
        };
        try {
            await AuthResolver.resetPassword(null, args);
        } catch (error) {
            expect(error.message).toBe('Reset password request has expired');
        }
    });
    it('Test Forgot Password, User not found', async () => {
        const fwData = await forgotPassword.model({
            Key: 'randomkey',
            Email: faker.internet.email().toLowerCase(),
            DateCreated: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
            DateExpiration: moment().utc().add(4, 'day').format('YYYY-MM-DD HH:mm:ss')
        }).save();
        const textInput = faker.internet.password(10, false, null, '0@4Fp##P');
        let args = {
            Key: fwData.Key,
            Password: textInput,
            ConfirmPassword: textInput,
        };
        try {
            await AuthResolver.resetPassword(null, args);
        } catch (error) {
            expect(error.message).toBe('User not found');
        }
    });
   it('Test Reset Password email, return success', (done) => {
       const dataforUser = {
           Email: faker.internet.email(),
           FirstName: faker.name.firstName('m'),
           LastName: faker.name.lastName('m'),
           Password: faker.internet.password(10, false, null, '0@4Fp##P'),
           Role: 'Admin',
           Status: 'Active',
           DateCreated: new Date(),
           DateUpdated: new Date()
       };
       const key = 'THISISKEYTEST';
       dataforUser.ConfirmPassword = dataforUser.Password;
       let args = dataforUser;
       UserResolver.addUser(null, args).then((user) => {
           expect(user._id).toBeDefined();
           expect(user.Email).toBe(dataforUser.Email.toLowerCase());
           forgotPassword.model({
               Key: key,
               Email: args.Email.toLowerCase(),
               DateCreated: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
               DateExpiration:  moment().utc().add(1,'day').format('YYYY-MM-DD HH:mm:ss')
           }).save().then(async (fwData) => {
               expect(fwData.Key).toBe(key);
               expect(fwData.Email).toBe(dataforUser.Email);
               const textInput = faker.internet.password(10, false, null, '0@4Fp##P');
               let args = {
                   Key: fwData.Key,
                   Password: textInput,
                   ConfirmPassword: textInput,
               };
               const data = await AuthResolver.resetPassword(null, args);
               expect(data.Email).toBe(dataforUser.Email);
               expect(data.FirstName).toBe(dataforUser.FirstName);
               expect(data.LastName).toBe(dataforUser.LastName);
               expect(data.Role).toBe(dataforUser.Role);
               expect(data.Status).toBe(dataforUser.Status);
               done();
           });

       });
   });
});