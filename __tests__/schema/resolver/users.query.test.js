const faker = require('faker');
const UserResolver = require('../../../schema/resolver/user');
const User = require('../../../models/users');
const { beforeEach, afterEach } = require('../../general');
const mongoose = require('mongoose');

describe('Test User Resolver query', () => {
    beforeAll(done => {
        jest.setTimeout(120000);
        beforeEach().then(() => done())
    });
    afterAll(done => afterEach().then(() => done()));
    it('Test get Users', async () => {
        const noOfRecords = 3;
        let i = 0;
        while (i < noOfRecords) {
            const inputSomething = faker.internet.password(10, false, null, '0@4Fp##P');
            const args = {
                FirstName: faker.name.firstName('m'),
                LastName: faker.name.lastName('m'),
                Email: faker.internet.email(),
                Status: 'Active',
                Password: inputSomething,
                ConfirmPassword: inputSomething,
                Role: 'SuperAdmin',
            };
            const user = new User.model(args);
            await user.save();
            expect(user).toBeDefined();
            i++;
        }

        const pageParams = {
            page: 0,
            pageSize: 5
        };

        const users = await UserResolver.getUsers(null, pageParams);
        expect(users.Total).toBe(noOfRecords);
        expect(users.PageSize).toBe(pageParams.pageSize);

    });
    it('Test get Users filtered', async () => {
        const noOfRecords = 3;
        let i = 0;
        let emailFilter = '';
        while (i < noOfRecords) {
            const inputSomething = faker.internet.password(10, false, null, '0@4Fp##P');
            const args = {
                FirstName: faker.name.firstName('m'),
                LastName: faker.name.lastName('m'),
                Email: faker.internet.email(),
                Status: 'Active',
                Password: inputSomething,
                ConfirmPassword: inputSomething,
                Role: 'SuperAdmin',
            };
            if (i === 0) {
                emailFilter = args.Email;
            }
            const user = new User.model(args);
            await user.save();
            expect(user).toBeDefined();
            i++;
        }

        const Role = 'SuperAdmin';
        const pageParams = {
            page: 0,
            pageSize: 5,
            filtered: [{ id: 'Email', value: emailFilter }, { id: 'Role', value: Role }],
            sorted: [{ id: 'Email', desc: true }],
        };

        const users = await UserResolver.getUsers(null, pageParams);
        expect(users.Total).toBe(1);
        expect(users.PageSize).toBe(pageParams.pageSize);
        expect(users.Users[0].Email).toBe(emailFilter);
        expect(users.Users[0].Role).toBe(Role);

    });
    it ('Test resolver get User with user not found', async () => {
        const fakeId = mongoose.Types.ObjectId();
        try {
            await UserResolver.getUser(null, { id: fakeId });
        } catch (e) {
            expect(e.message).toStrictEqual('User not found.');
        }
    });
    it('Test Resolver User get User query', async () => {
        const inputSomething = faker.internet.password(10, false, null, '0@4Fp##P');
        const args = {
            FirstName: faker.name.firstName('m'),
            LastName: faker.name.lastName('m'),
            Email: faker.internet.email(),
            Status: 'Active',
            Password: inputSomething,
            ConfirmPassword: inputSomething,
            Role: 'SuperAdmin',
        };
        let user = new User.model(args);
        user = await user.save();

        const userObject = await UserResolver.getUser(null, { id: user._id });
        expect(userObject.id.toString()).toBe(user._id.toString());
        expect(userObject.FirstName).toBe(args.FirstName);
    });
});