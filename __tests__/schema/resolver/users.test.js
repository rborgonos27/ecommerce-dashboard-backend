const mongoose = require('mongoose');
const faker = require('faker');
const UserResolver = require('../../../schema/resolver/user');
const { beforeEach, afterEach } = require('../../general');

describe('User Resolver tests', () => {
    beforeAll(done => {
        jest.setTimeout(120000);
        beforeEach().then(() => done())
    });
    afterAll(done => afterEach().then(() => done()));
    it('Test AddUser resolver', async () => {
        const inputText = faker.internet.password(8, true, null, '0@P');
        const args = {
            FirstName: faker.name.firstName('m'),
            LastName: faker.name.lastName('m'),
            Email: faker.internet.email(),
            Status: 'Active',
            Password: inputText,
            ConfirmPassword: inputText,
            Role: 'SuperAdmin',
        };
        const user = await UserResolver.addUser(null, args);
        const userargs = [user, args];
        expect(userargs).toBeDefined();
        expect(user._id).toBeDefined();
        expect(user.FirstName).toBe(args.FirstName);
        expect(user.LastName).toBe(args.LastName);
        expect(user.Email).toBe(args.Email);
        expect(user.Role).toBe(args.Role);
        expect(user.Password).toBeDefined();
        expect(user.ConfirmPassword).toBeUndefined();
    });
    it('Test resolver email validation', async () => {
        const inputText = faker.internet.password(8, true, null, '0@P');
        const args = {
            FirstName: faker.name.firstName('m'),
            LastName: faker.name.lastName('m'),
            Email: "test",
            Status: 'Active',
            Password: inputText,
            ConfirmPassword: inputText,
            Role: 'SuperAdmin',
        };

        const user = () => UserResolver.addUser(null, args);
        expect(user).toThrow(Error);
    });
    it('Test resolver password and confirm password validation', async () => {
        const inputText = faker.internet.password(8, true, null, '0@P');
        const args = {
            FirstName: faker.name.firstName('m'),
            LastName: faker.name.lastName('m'),
            Email: faker.internet.email(),
            Status: 'Active',
            Password: inputText,
            ConfirmPassword: faker.internet.password(10, true, null, '0@12P'),
            Role: 'SuperAdmin',
        };

        const user = () => UserResolver.addUser(null, args);
        expect(user).toThrow(Error);
    });
    it('Test resolver validation password is empty', async () => {
        const args = {
            FirstName: faker.name.firstName('m'),
            LastName: faker.name.lastName('m'),
            Email: faker.internet.email(),
            Status: 'Active',
            Password: '',
            ConfirmPassword: '',
            Role: 'SuperAdmin',
        };

        const user = () => UserResolver.addUser(null, args);
        expect(user).toThrow(Error);
    });
    it('Test resolver validation password is invalid', async () => {
        const inputText = faker.internet.password(5);
        const args = {
            FirstName: faker.name.firstName('m'),
            LastName: faker.name.lastName('m'),
            Email: faker.internet.email(),
            Status: 'Active',
            Password: inputText,
            Role: 'SuperAdmin',
        };

        const user = () => UserResolver.addUser(null, args);
        expect(user).toThrow(Error);
    });
    it('Test resolver add user with existing user', async () => {
        const inputText = faker.internet.password(10, false, null, '0@4Fp##P');
        const email = faker.internet.email();
        const args = {
            FirstName: faker.name.firstName('m'),
            LastName: faker.name.lastName('m'),
            Email: email,
            Status: 'Active',
            Password: inputText,
            ConfirmPassword: inputText,
            Role: 'SuperAdmin',
        };
        const user = await UserResolver.addUser(null, args);
        const userargs = [user, args];
        expect(userargs).toBeDefined();
        args.FirstName = faker.name.firstName('m');
        delete args.DateCreated;
        delete args.DateUpdated;
        args.Password = inputText;
        args.ConfirmPassword = inputText;
        try {
            await UserResolver.addUser(null, args);
        } catch (e) {
            expect(e.extensions).toStrictEqual({ formError: { Email: 'Email already exists.' } });
        }
    });
    it('Test resolver update user with user not found.', async () => {
        const inputText = faker.internet.password(10, false, null, '0@4Fp##P');
        const email = faker.internet.email();
        const fakeId = mongoose.Types.ObjectId();
        const args = {
            id: fakeId,
            FirstName: faker.name.firstName('m'),
            LastName: faker.name.lastName('m'),
            Email: email,
            Status: 'Active',
            Password: inputText,
            ConfirmPassword: inputText,
            Role: 'SuperAdmin',
        };
        try {
            await UserResolver.updateUser(null, args);
        } catch (e) {
            expect(e.message).toStrictEqual('User not found.');
        }
    });
    it('Test resolver UpdateUser', async () => {
        const inputText = faker.internet.password(10, false, null, '0@4Fp##P');
        const args = {
            FirstName: faker.name.firstName('m'),
            LastName: faker.name.lastName('m'),
            Email: faker.internet.email(),
            Status: 'Active',
            Password: inputText,
            ConfirmPassword: inputText,
            Role: 'SuperAdmin',
        };
        const user = await UserResolver.addUser(null, args);
        const userargs = [user, args];
        expect(userargs).toBeDefined();
        args.id = user._id;
        args.FirstName = faker.name.firstName('m');
        delete args.DateCreated;
        delete args.DateUpdated;
        args.Password = inputText;
        args.ConfirmPassword = inputText;
        args.ConfirmPassword = inputText;
        const updateUser = await UserResolver.updateUser(null, args);
        expect(args.FirstName).toBe(updateUser.FirstName);
    });
    it('Test resolver UpdateUser validation error', async () => {
        const inputText = faker.internet.password(10, false, null, '0@4Fp##P');
        const args = {
            FirstName: faker.name.firstName('m'),
            LastName: faker.name.lastName('m'),
            Email: faker.internet.email(),
            Status: 'Active',
            Password: inputText,
            ConfirmPassword: inputText,
            Role: 'SuperAdmin',
        };
        const user = await UserResolver.addUser(null, args);
        const userargs = [user, args];
        expect(userargs).toBeDefined();
        args.id = user._id;
        args.FirstName = faker.name.firstName('m');
        delete args.DateCreated;
        delete args.DateUpdated;
        delete args.Password;
        const inputSomething = 'aa.aa';
        args.Password = inputSomething;
        args.ConfirmPassword = inputSomething;
        const updateUser = () => UserResolver.updateUser(null, args);
        expect(updateUser).toThrow(Error);
    });
    it ('Test resolver Delete User user not found', async () => {
        const fakeId = mongoose.Types.ObjectId();
        try {
            await UserResolver.deleteUser(null, {id: fakeId});
        } catch (e) {
            expect(e.message).toStrictEqual('User not found.');
        }
    });
    it('Test resolver Delete User', async () => {
        const inputText = faker.internet.password(10, false, null, '0@4Fp##P');
        const args = {
            FirstName: faker.name.firstName('m'),
            LastName: faker.name.lastName('m'),
            Email: faker.internet.email(),
            Status: 'Active',
            Password: inputText,
            ConfirmPassword: inputText,
            Role: 'SuperAdmin',
        };
        const user = await UserResolver.addUser(null, args);
        const userargs = [user, args];
        expect(userargs).toBeDefined();
        expect(user._id).toBeDefined();
        const deleteUser = await UserResolver.deleteUser(null, {id: user._id});
        expect(deleteUser.Success).toBeTruthy();
        // const notExists = () => UserResolver.deleteUser(null, {id: user._id});
        // expect(notExists).toThrow(Error);
    });
});
