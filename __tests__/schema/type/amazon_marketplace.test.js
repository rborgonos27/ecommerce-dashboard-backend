const AmazonMarketplace= require('../../../schema/types/amazon_marketplace');

describe('Schema Type Amazon Marketplace test', () => {
   it ('Amazon Marketplace key value', () => {
       const USMarketplace = AmazonMarketplace.getValue('US');
       expect(USMarketplace.name).toBe('US');
       expect(USMarketplace.value).toHaveProperty('CountryCode');
       expect(USMarketplace.value).toHaveProperty('EndPoint');
       expect(USMarketplace.value).toHaveProperty('MarketPlaceId');
       expect(AmazonMarketplace.name).toBe('AmazonMarketplace');
   });
});