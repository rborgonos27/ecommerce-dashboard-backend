const express = require('express');
const ExpressGraphQL = require("express-graphql");
const router = express.Router();
const authSchema = require('../schema/auth');

router.use("/graphql", ExpressGraphQL({
    schema: authSchema,
    graphiql: false
}));

router.use("/iql", ExpressGraphQL({
    schema: authSchema,
    graphiql: true
}));

module.exports = router;