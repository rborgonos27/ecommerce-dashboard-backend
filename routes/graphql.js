const express = require('express');
const ExpressGraphQL = require("express-graphql");
const router = express.Router();
const schema = require('../schema');

router.use("/graphql", ExpressGraphQL({
    schema,
    graphiql: false
}));

router.use("/iql", ExpressGraphQL({
    schema,
    graphiql: true
}));

module.exports = router;