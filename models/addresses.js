const mongoose = require('mongoose');
const { GraphQLObjectType } = require('graphql');
const Columns = require('../utils/columns');
const Schema = mongoose.Schema;
const GraphQLDate = require('graphql-date');

const Address = {};

Address.design = {
    label: 'Address',
    cols: {
        'Nickname': { type: String },
        'ShippingName': { type: String },
        'Address1': { type: String },
        'Address2': { type: String },
        'City': { type: String },
        'State': { type: String },
        'PostalCode': { type: String },
        'PhoneNumber': { type: String },
        'DateCreated': { type: Date, graphQL: GraphQLDate },
        'DateUpdated': { type: Date, graphQL: GraphQLDate },
    }
};

Address.schema = new Schema(Address.design.cols);
Address.model = mongoose.model('Address', Address.schema, 'Address');
Address.type = new GraphQLObjectType({
    name: 'Address',
    fields: Columns.GenerateGraphQlSchema(Address.design.cols, false)
});
module.exports = Address;
