const mongoose = require('mongoose');
const { GraphQLObjectType, GraphQLInt, GraphQLID,  GraphQLString, GraphQLBoolean } = require('graphql');
const Columns = require('../utils/columns');
const Schema = mongoose.Schema;
const GraphQLDate = require('graphql-date');
const Currency = require('../schema/types/currency');
const ProductInfo = require('../schema/types/product_info');

const OrderItem = {};

OrderItem.design = {
    label: 'OrderItem',
    cols: {
        'ClientId': { type: String },
        'AmazonOrderId': { type: String },
        'QuantityOrdered': { type: Number , graphQL: GraphQLInt },
        'Title' : { type: String },
        'PromotionDiscount' : { type: Object, graphQL: Currency },
        'ASIN' : { type: String },
        'SellerSKU' : { type: String },
        'OrderItemId' : { type: String },
        'PromotionIds' : { type: Object },
        'IsTransparency' : { type: Boolean, graphQL: GraphQLBoolean },
        'ProductInfo' : { type: Object, graphQL: ProductInfo },
        'QuantityShipped' : { type: Number },
        'ItemPrice' : { type: Object, graphQL: Currency },
        'ItemTax' : { type: Object, graphQL: Currency },
        'PromotionDiscountTax' : { type: Object, graphQL: Currency },
        'DateUpdated' : { type: Date, graphQL: GraphQLDate },
        'DateCreated' : { type: Date, graphQL: GraphQLDate },
        'DateSynced': { type: Date },
    }
};

OrderItem.schema = new Schema(Columns.GenerateSchema(OrderItem.design.cols));
OrderItem.model = mongoose.model('OrderItem', OrderItem.schema, 'OrderItem');
const graphqlFields = Columns.GenerateGraphQlSchema(OrderItem.design.cols, false, ['PromotionIds']);
OrderItem.type = new GraphQLObjectType({
    name: 'OrderItem',
    fields: graphqlFields
});
module.exports = OrderItem;
