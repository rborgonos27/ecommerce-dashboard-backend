const mongoose = require('mongoose');
const { GraphQLInt, GraphQLObjectType, GraphQLList, GraphQLString } = require('graphql');
const Columns = require('../utils/columns');
const Schema = mongoose.Schema;
const GraphQLDate = require('graphql-date');

const User = {};

User.design = {
    label: 'User',
    cols: {
        'Email': {type: String},
        'FirstName': {type: String},
        'LastName': {type: String},
        'Password': {type: String},
        'Role': {type: String},
        'Status': {type: String},
        'DateCreated': { type: Date, graphQL: GraphQLDate },
        'DateUpdated': { type: Date, graphQL: GraphQLDate },
        'DateDeleted': { type: Date, graphQL: GraphQLDate },
        'Total': {type: Number, graphQL: GraphQLInt},
        'PageSize': {type: Number, graphQL: GraphQLInt}
    }
};

User.schema = new Schema(Columns.GenerateSchema(User.design.cols));
User.model = mongoose.model('User', User.schema, 'User');
User.type = new GraphQLObjectType({
    name: 'User',
    fields: Columns.GenerateGraphQlSchema(
        User.design.cols,
        false,
        ['Password', 'DateDeleted']
    )
});
module.exports = User;
