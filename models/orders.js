const mongoose = require('mongoose');
const { GraphQLObjectType, GraphQLInt, GraphQLList,  GraphQLFloat, GraphQLString, GraphQLBoolean } = require('graphql');
const Columns = require('../utils/columns');
const OrderItem = require('./order_items');
const Schema = mongoose.Schema;
const GraphQLDate = require('graphql-date');

const Order = {};

Order.design = {
    label: 'Order',
    cols: {
        'ClientId': { type: String },
        'OrderType': { type: String },
        'PurchaseDate': { type: Date, graphQL: GraphQLDate },
        'BuyerEmail': { type: String },
        'AmazonOrderId': { type: String },
        'LastUpdateDate': { type: Date, graphQL: GraphQLDate },
        'IsReplacementOrder': { type: Boolean, graphQL: GraphQLBoolean },
        'NumberOfItemsShipped': { type: Number, graphQL: GraphQLInt },
        'ShipServiceLevel': { type: String, graphQL: GraphQLString },
        'OrderStatus': { type: String, graphQL: GraphQLString },
        'SalesChannel': { type: String, graphQL: GraphQLString },
        'IsBusinessOrder': { type: Boolean, graphQL: GraphQLBoolean },
        'NumberOfItemsUnshipped': { type: Number, graphQL: GraphQLInt },
        'IsGlobalExpressEnabled': { type: Boolean, graphQL: GraphQLBoolean },
        'IsSoldByAB': { type: Boolean, graphQL: GraphQLBoolean },
        'IsPremiumOrder': { type: Boolean, graphQL: GraphQLBoolean },
        'OrderTotal': { type: Object, graphQL: new GraphQLObjectType({
                name: 'AmountValue',
                fields: {
                    'Amount': { type: GraphQLFloat },
                    'CurrencyCode': { type: GraphQLString }
                }
            }) },
        'EarliestShipDate': { type: Date, graphQL: GraphQLDate },
        'MarketplaceId': { type: String },
        'FulfillmentChannel': { type: String },
        'PaymentMethod': { type: String },
        'ShippingAddress': { type: Object },
        'IsPrime': { type: Boolean, graphQL: GraphQLBoolean },
        'SellerOrderId': { type: String },
        'ShipmentServiceLevelCategory': { type: String },
        'OrderItem': { graphQL: GraphQLList(OrderItem.type) },
        'DateUpdated' : { type: Date, graphQL: GraphQLDate },
        'DateCreated' : { type: Date, graphQL: GraphQLDate },
        'DateSynced': { type: Date },
    }
};

Order.schema = new Schema(Columns.GenerateSchema(Order.design.cols, false, ['OrderItem']));
Order.model = mongoose.model('Order', Order.schema, 'Order');
const graphqlFields = Columns.GenerateGraphQlSchema(Order.design.cols, false);
Order.type = new GraphQLObjectType({
    name: 'Order',
    fields: graphqlFields
});
module.exports = Order;
