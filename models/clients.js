const mongoose = require('mongoose');
const { GraphQLString } = require('graphql');
const { ObjectId } = mongoose.Types;
const { GraphQLObjectType, GraphQLList, GraphQLBoolean } = require('graphql');
const User = require('./users');
const Address = require('./addresses');
const Columns = require('../utils/columns');
const Schema = mongoose.Schema;
const GraphQLDate = require('graphql-date');

const Client = {};

Client.design = {
    label: 'Client',
    cols: {
        'Name': {type: String},
        'Users': {type: [Object], graphQL: GraphQLList(User.type)},
        'UserIds': {type: [ObjectId]},
        'MarketPlace': {type: [Object], graphQL: GraphQLList(new GraphQLObjectType({
                name: 'MarketPlace',
                fields: {
                    'CountryCode': { type: GraphQLString },
                    'EndPoint': { type: GraphQLString },
                    'MarketPlaceId': { type: GraphQLString }
                }
            }))},
        'SellerId': {type: String},
        'MwsAuthToken': {type: String},
        'IsSetupComplete': {type: Boolean, graphQL: GraphQLBoolean },
        'Addresses': {type: [Object], graphQL: GraphQLList(Address.type)},
        'AddressIds': {type: [ObjectId]},
        'DateCreated': { type: Date, graphQL: GraphQLDate },
        'DateUpdated': { type: Date, graphQL: GraphQLDate },
        'DateDeleted': { type: Date, graphQL: GraphQLDate },
    }
};

Client.schema = new Schema(Columns.GenerateSchema(Client.design.cols));
Client.model = mongoose.model('Client', Client.schema, 'Client');
const graphqlFields = Columns.GenerateGraphQlSchema(Client.design.cols,
    false,
    ['AddressIds', 'UserIds', 'ClientUserIds', 'DateDeleted']
);
Client.type = new GraphQLObjectType({
    name: 'Client',
    fields: graphqlFields
});
module.exports = Client;
