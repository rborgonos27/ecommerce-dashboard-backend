const Address = require('./addresses');
const Client = require('./clients');
const Item = require('./items');
const Order = require('./orders');
const User = require('./users');

module.exports = {
    Address,
    Client,
    Item,
    Order,
    User,
};
