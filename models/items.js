const mongoose = require('mongoose');
const { GraphQLObjectType, GraphQLInt, GraphQLBoolean, GraphQLList } = require('graphql');
const GraphQLDate = require('graphql-date');
const Columns = require('../utils/columns');
const Schema = mongoose.Schema;
const SalesRank = require('../schema/types/sales_rank');
const Dimension = require('../schema/types/dimension');
const ValueUnit = require('../schema/types/value_unit');
const Image = require('../schema/types/image');

const Item = {};

Item.design = {
    label: 'Item',
    cols: {
        'ClientId': { type: String },
        'ItemId': { type: String },
        'MarketPlaceId': { type: String },
        'ASIN': { type: String },
        'Brand': { type: String },
        'Binding': { type: String },
        'Genre': { type: String },
        'ItemDimensions': { type: Object, graphQL: Dimension },
        'IsAdultProduct': { type: Boolean, graphQL: GraphQLBoolean },
        'Label': { type: String },
        'LastPrice': { type: Object },
        'Manufacturer': { type: String },
        'ManufacturerMinimumAge': { type: Object, graphQL: ValueUnit('ManufacturerMinimumAge') },
        'MaterialType': { type: String },
        'PackageDimensions': { type: Object, graphQL: Dimension },
        'PackageQuantity': { type: String, graphQL: GraphQLInt },
        'PartNumber': { type: String },
        'ProductGroup': { type: String },
        'ProductTypeName': { type: String },
        'Publisher': { type: String },
        'SmallImage': { type: Object, graphQL: Image },
        'Studio': { type: String },
        'Title': { type: String },
        'SalesRank': { type: [Object], graphQL: GraphQLList(SalesRank) },
        'DateSynced': { type: Date, graphQL: GraphQLDate },
        'DateUpdated' : { type: Date, graphQL: GraphQLDate },
        'DateCreated' : { type: Date, graphQL: GraphQLDate },
    }
};

Item.schema = new Schema(Columns.GenerateSchema(Item.design.cols));
Item.model = mongoose.model('Item', Item.schema, 'Item');
const graphqlFields = Columns.GenerateGraphQlSchema(Item.design.cols, false);
Item.type = new GraphQLObjectType({
    name: 'Item',
    fields: graphqlFields
});
module.exports = Item;
