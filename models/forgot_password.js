const mongoose = require('mongoose');
const { GraphQLObjectType } = require('graphql');
const Columns = require('../utils/columns');
const Schema = mongoose.Schema;
const GraphQLDate = require('graphql-date');

const ForgotPassword = {};

ForgotPassword.design = {
    label: 'ForgotPassword',
    cols: {
        'Key': {type: String},
        'Email': {type: String},
        'DateCreated': { type: Date, graphQL: GraphQLDate },
        'DateExpiration': { type: Date, graphQL: GraphQLDate },
    }
};

ForgotPassword.schema = new Schema(Columns.GenerateSchema(ForgotPassword.design.cols));
ForgotPassword.model = mongoose.model('ForgotPassword', ForgotPassword.schema, 'ForgotPassword');
const graphqlFields = Columns.GenerateGraphQlSchema(ForgotPassword.design.cols);
ForgotPassword.type = new GraphQLObjectType({
    name: 'ForgotPassword',
    fields: graphqlFields
});

module.exports = ForgotPassword;
