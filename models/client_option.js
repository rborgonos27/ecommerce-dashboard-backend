const mongoose = require('mongoose');
const { GraphQLObjectType } = require('graphql');
const Columns = require('../utils/columns');
const Schema = mongoose.Schema;
const GraphQLDate = require('graphql-date');

const ClientOption = {};

ClientOption.design = {
    label: 'ClientOption',
    cols: {
        'ClientId': {type: String},
        'CanSync': {type: Boolean},
        'DateLastRecord': { type: Date, graphQL: GraphQLDate },
        'DateLastSynced': { type: Date, graphQL: GraphQLDate },
        'DateCreated': { type: Date, graphQL: GraphQLDate },
        'DateUpdated': { type: Date, graphQL: GraphQLDate },
    }
};

ClientOption.schema = new Schema(ClientOption.design.cols);
ClientOption.model = mongoose.model('ClientOption', ClientOption.schema, 'ClientOption');
ClientOption.type = new GraphQLObjectType({
    name: 'ClientOption',
    fields: Columns.GenerateGraphQlSchema(ClientOption.design.cols, false, ['Password'])
});
module.exports = ClientOption;
