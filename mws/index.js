require('dotenv').config();
const mws = require('amazon-mws')(process.env.AWS_ACCESS_KEY_ID, process.env.AWS_SECRET_ACCESS_KEY);
mws.setResponseFormat('json');
const _ = require('lodash');
const Client = require('../models/clients');
const Order = require('../models/orders');
const OrderItem = require('../models/order_items');
const ClientOption = require('../models/client_option');
const Item = require('../models/items');
const { isEmptyObject } = require('../utils/helper');
const moment = require('moment');

let lastUpdatedAt = null;
const getOrders = () => {
    Client.model.find({ DateDeleted: null }).exec().then(async(res) => {
       res.map(async client => {
           console.log(`Syncing OrderItem for client ${client.Name}...`);
           const clientOption = await ClientOption.model.findOne({ ClientId: client._id });
           console.log('client option', clientOption);
           let lastUpdatedAfter =  new Date(2013,1,1);
           if (clientOption) {
               const { DateLastRecord } = clientOption;
               console.log ('data last record', DateLastRecord);
               lastUpdatedAfter = DateLastRecord;
           }
           const config = {
               Version: '2013-09-01',
               Action: 'ListOrders',
               SellerId: client.SellerId,
               MWSAuthToken: client.MwsAuthToken,
               LastUpdatedAfter: lastUpdatedAfter
           };
           const marketplace = {};
           let ctr = 1;
           client.MarketPlace.map(marketplaceObject => {
               marketplace[`MarketplaceId.Id.${ctr}`] = marketplaceObject.MarketPlaceId;
               ctr++;
           });
           const orderConfig = _.merge({}, config, marketplace);
           console.log(orderConfig);
           mws.orders.search(orderConfig, async function (error, response) {
               if (error) {
                   console.log('Mws Error ', error);
                   return;
               }
               console.log('response', response);

               let orders = response.Orders;
               console.log(orders, 'orders');
               await saveOrders(orders, client);
               let nextToken = response.NextToken ? response.NextToken : null;
               try {
                   if (nextToken) {
                       do {
                           const configNextToken = {
                               Version: '2013-09-01',
                               Action: 'ListOrdersByNextToken',
                               SellerId: client.SellerId,
                               MWSAuthToken: client.MwsAuthToken,
                               NextToken: nextToken
                           };
                           console.log('next token', configNextToken);
                           const responseData = await mws.orders.search(configNextToken);
                           console.log('response next Token', responseData);
                           nextToken = responseData.NextToken ? responseData.NextToken : null;
                           orders = responseData.Orders;
                           await saveOrders(orders, client);
                       } while (nextToken !== null);
                   }
               } catch (error) {
                   console.log('sync error', error);
               }
               // last update
               await updateClientOption(client);
               console.log('Sync done......');
           });
       });
    });
};

updateClientOption = async (client) => {
    const dateNow = moment().utc().format('YYYY-MM-DD HH:mm:ss');
    const clientOptionParam = {
        ClientId: client._id,
        CanSync: true,
        DateLastSynced: dateNow,
        DateCreated: dateNow,
        DateUpdated: dateNow,
    };
    if (lastUpdatedAt) clientOptionParam.DateLastRecord = lastUpdatedAt;
    console.log(clientOptionParam, 'client Option');
    await ClientOption.model.findOne( { ClientId: client._id }).exec().then( async clientOption => {
        if (clientOption !== null) {
            return ClientOption.model.findOneAndUpdate( {_id: clientOption._id}, clientOptionParam, { new: true } );
        } else {
            return ClientOption.model(clientOptionParam).save();
        }
    });
};

saveOrders = async (orders, client) => {
    if (!isEmptyObject(orders)) {
        orders.Order.map(async order => {
            Order.model.findOne({ AmazonOrderId: order.AmazonOrderId }).exec().then(async res => {
                console.log(res);
                lastUpdatedAt = order.LastUpdateDate;
                if (res === null) {
                    const orderParam = order;
                    orderParam.ClientId = client._id;
                    const orderObject = new Order.model(orderParam);
                    await orderObject.save();
                }
            });
        });
    }
};

getOrderItems = async (orderId, client) => {
    mws.orders.search({
        Version: '2013-09-01',
        Action: 'ListOrderItems',
        SellerId: client.SellerId,
        MWSAuthToken: client.MwsAuthToken,
        AmazonOrderId: orderId,
    }, async function (error, response) {
        if (error) {
            console.log('error ', error);
            return;
        }
        console.log('response', response);
        console.log('OrderItemId', response.OrderItems.OrderItem.OrderItemId);
        console.log('PromotionId', response.OrderItems.OrderItem.PromotionIds);
        console.log('ProductInfo', response.OrderItems.OrderItem.ProductInfo);
        console.log('PromotionDiscount', response.OrderItems.OrderItem.PromotionDiscount);
        console.log('PromotionDiscountTax', response.OrderItems.OrderItem.PromotionDiscountTax);
        console.log('ItemPrice', response.OrderItems.OrderItem.ItemPrice);
        console.log('Item Tax', response.OrderItems.OrderItem.ItemTax);
        const orderItem = response.OrderItems.OrderItem;
        orderItem.AmazonOrderId = orderId;
        orderItem.ClientId = client._id;

        OrderItem.model.findOne({ OrderItemId: orderItem.OrderItemId }).exec().then(async res => {
            console.log(res);
            if (res === null) {
                console.log('addding', res);
                const orderItemObject = new OrderItem.model(orderItem);
                await orderItemObject.save();
            }
        });
    });
};

const getOrderDetails = async () => {
    Client.model.find({ DateDeleted: null }).exec().then(async(res) => {
        res.map(async(client) => {
            console.log(`Syncing OrderItem for client ${client.Name}...`);
            const amazonOrderId = await OrderItem.model.distinct('AmazonOrderId');
            console.log('amazonOrderId', amazonOrderId);
            const orders = await Order.model
                .find({ ClientId: client._id, AmazonOrderId: { $nin: amazonOrderId} })
                .limit(100);
            console.log(orders.length);
            console.log(amazonOrderId.length);
            orders.map(async order => {
                console.log(order.AmazonOrderId);
                await getOrderItems(order.AmazonOrderId, client);
            });
        });
    });
};

const getProductItemDetails = (client, itemId) => {
    const productConfig = {
        Version: '2011-10-01',
        Action: 'GetMatchingProductForId',
        SellerId: client.SellerId,
        MWSAuthToken: client.MwsAuthToken,
        IdType: 'SellerSKU',
        MarketplaceId: client.MarketPlace[0].MarketPlaceId,
        'IdList.Id.1': itemId
    };
    console.log(productConfig);
    return mws.products.search(productConfig);
};

const getReportData = () => {
    const productConfig = {
        Version: '2009-01-01',
        Action: 'RequestReport',
        SellerId: 'A3SVXPLZ1T1AVS',
        MWSAuthToken: 'amzn.mws.e0b89dbd-e1f7-c6ef-18a6-e7690b7ae343',
        ReportType: '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_'
    };
    console.log(productConfig);
    mws.reports.submit(productConfig, function (error, response) {
        console.log(response);
        console.log('report request id', response.ReportRequestInfo.ReportRequestId);
        mws.reports.search({
            'Version': '2009-01-01',
            'Action': 'GetReport',
            SellerId: 'A3SVXPLZ1T1AVS',
            MWSAuthToken: 'amzn.mws.e0b89dbd-e1f7-c6ef-18a6-e7690b7ae343',
            'ReportId': response.ReportRequestInfo.ReportRequestId,
        }, function (error, response) {
            if (error) {
                console.log('error ', error);
                return;
            }
            resolve(response.data);
        });
    });
};

// const getProductItemDetailsAsin = () => {
//     const productConfig = {
//         Version: '2011-10-01',
//         Action: 'GetMatchingProduct',
//         SellerId: 'A3SVXPLZ1T1AVS',
//         MWSAuthToken: 'amzn.mws.e0b89dbd-e1f7-c6ef-18a6-e7690b7ae343',
//         MarketplaceId: 'ATVPDKIKX0DER',
//         'ASINList.ASIN.1': 'B06ZYW5RY8'
//     };
//     console.log(productConfig);
//     mws.products.search(productConfig, function (error, response) {
//         console.log(response, error);
//         console.log('response', response.Product.Identifiers);
//         console.log('response', response.Product.AttributeSets);
//         console.log('Item Dimensions', response.Product.AttributeSets.ItemAttributes.ItemDimensions);
//         console.log('Item Dimensions', response.Product.AttributeSets.ItemAttributes.ItemDimensions.Length);
//         console.log('Item Dimensions', response.Product.AttributeSets.ItemAttributes.ItemDimensions.Width);
//         console.log('Item Dimensions', response.Product.AttributeSets.ItemAttributes.ItemDimensions.Weight);
//         console.log('response', response.Product.SalesRankings);
//     });
// };

const getProductDetails = () => {
    const dateNow = moment().utc().format('YYYY-MM-DD HH:mm:ss');
    console.log('Pulling ListOrders from MWS api...');
    Client.model.find({ DateDeleted: null }).exec().then(async(res) => {
        res.map(async(client) => {
            console.log(`Syncing OrderItem for client ${client.Name}...`);
            const orderItems = await OrderItem.model.distinct('SellerSKU', { ClientId: client._id });
            orderItems.map(async (SellerSKU) => {
                    Item.model.findOne({ ItemId: SellerSKU }).exec().then(
                        async(item) => {
                            if (item == null) {
                                const mwsItem = await getProductItemDetails(client, SellerSKU);
                                console.log(mwsItem);
                                if (mwsItem && mwsItem.status === 'Success') {
                                    console.log('response', mwsItem);
                                    console.log('response', mwsItem.Products.Product.Identifiers);
                                    console.log('response', mwsItem.Products.Product.AttributeSets);
                                    console.log('Item Dimensions', mwsItem.Products.Product.AttributeSets.ItemAttributes.ItemDimensions);
                                    console.log('Item Dimensions', mwsItem.Products.Product.AttributeSets.ItemAttributes.ItemDimensions.Length);
                                    console.log('Item Dimensions', mwsItem.Products.Product.AttributeSets.ItemAttributes.ItemDimensions.Width);
                                    console.log('Item Dimensions', mwsItem.Products.Product.AttributeSets.ItemAttributes.ItemDimensions.Weight);
                                    console.log('response', mwsItem.Products.Product.SalesRankings);
                                    const {
                                        AttributeSets,
                                        Identifiers,
                                        SalesRankings
                                    } = mwsItem.Products.Product;
                                    const {
                                        MarketplaceASIN
                                    } = Identifiers;
                                    console.log('attributeSets', AttributeSets);
                                    // marketplace ASIN
                                    const {
                                        MarketplaceId,
                                        ASIN,
                                    } = MarketplaceASIN;
                                    const {
                                        Brand,
                                        Genre,
                                        Binding,
                                        ItemDimensions,
                                        IsAdultProduct,
                                        Label,
                                        LastPrice,
                                        Manufacturer,
                                        ManufacturerMinimumAge,
                                        MaterialType,
                                        PackageDimensions,
                                        PackageQuantity,
                                        PartNumber,
                                        ProductGroup,
                                        ProductTypeName,
                                        Publisher,
                                        SmallImage,
                                        Studio,
                                        Title,
                                    } = AttributeSets.ItemAttributes;
                                    const args = {
                                        ClientId: 'ClientId',
                                        ItemId: mwsItem.Id,
                                        MarketPlaceId: MarketplaceId,
                                        ASIN,
                                        Brand,
                                        Binding,
                                        Genre,
                                        ItemDimensions,
                                        IsAdultProduct,
                                        Label,
                                        LastPrice,
                                        Manufacturer,
                                        ManufacturerMinimumAge,
                                        MaterialType,
                                        PackageDimensions,
                                        PackageQuantity,
                                        PartNumber,
                                        ProductGroup,
                                        ProductTypeName,
                                        Publisher,
                                        SmallImage,
                                        Studio,
                                        Title,
                                        SalesRank: SalesRankings.SalesRank,
                                        DateSynced: dateNow,
                                        DateUpdated: dateNow,
                                        DateCreated: dateNow,
                                    };
                                    // save item
                                    const productItem = await Item.model(args).save();
                                    console.log(productItem);
                                } else if (mwsItem && mwsItem.status === 'ClientError') {
                                    console.log('response', mwsItem);
                                    console.log('response error', mwsItem.Error);
                                    return false;
                                }

                            } else {
                                return false;
                            }
                        }
                    );
                    console.log('orderItem', SellerSKU);
            });
        });
    });
};

module.exports = {
    mws,
    getOrders,
    getOrderDetails,
    getProductDetails,
    updateClientOption,
    lastUpdatedAt,
    // getProductItemDetailsAsin,
    getReportData
};
