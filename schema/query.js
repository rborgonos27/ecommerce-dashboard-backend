const graphql = require('graphql');
const { GraphQLObjectType, GraphQLList, GraphQLInt, GraphQLID, GraphQLNonNull } = graphql;
const { sorted, filtered, UserList, ClientList, OrderList, ItemList } = require('./types');
const User = require('../models/users');
const Client = require('../models/clients');
const Address = require('../models/addresses');
const Item = require('../models/items');
const { ObjectId } = require('mongoose').Types;
const { getUsers, getUser } = require('./resolver/user');
const { getOrders } = require('./resolver/orders');
const { getItems, getItem } = require('./resolver/item');

const Query = new GraphQLObjectType({
    name: 'Queries',
    fields: () => ({
        user: {
            type: User.type,
            args: { id: {type: GraphQLNonNull(GraphQLID)} },
            resolve: getUser
        },
        users: {
            type: UserList,
            args:{
                page: { type: GraphQLNonNull(GraphQLInt) },
                pageSize: { type: GraphQLNonNull(GraphQLInt) },
                sorted: { type: GraphQLList(sorted) },
                filtered: { type: GraphQLList(filtered) }
            },
            resolve: getUsers
        },
        clients: {
            type: ClientList,
            args:{
                page: { type: GraphQLNonNull(GraphQLInt) },
                pageSize: { type: GraphQLNonNull(GraphQLInt) },
                sorted: { type: GraphQLList(sorted) },
                filtered: { type: GraphQLList(filtered) }
            },
            resolve(parentValue, args) {
                let sort = {};
                if (args.sorted !== undefined && args.sorted.length)
                    sort[args.sorted[0].id] = args.sorted[0].desc ? -1 : 1;
                else
                    sort = {DateCreated: -1};

                let filter = { DateDeleted: null };
                if (args.filtered !== undefined && args.filtered.length)
                    filter[args.filtered[0].id] = args.filtered[0].value;

                return Client.model
                    .aggregate(
                        [
                            {
                                '$lookup': {
                                    'from': User.model.collection.name,
                                    'localField':  'UserIds',
                                    'foreignField': '_id',
                                    'as': 'Users',
                                }
                            },
                            {
                                '$lookup': {
                                    'from': Address.model.collection.name,
                                    'localField':  'AddressIds',
                                    'foreignField': '_id',
                                    'as': 'Addresses',
                                }
                            },
                            {
                                '$match': { DateDeleted: null }
                            },
                        ]
                    )
                    .skip(args.page * args.pageSize)
                    .limit(args.pageSize)
                    .sort(sort)
                    .exec().then(res =>{
                        return Client.model.countDocuments({ DateDeleted: null }).exec().then(count => {
                            return {
                                Total: count,
                                Clients: res,
                                PageSize: args.pageSize
                            }
                        })
                    })
            }
        },
        client: {
            type: Client.type,
            args: { id: {type: GraphQLNonNull(GraphQLID)} },
            resolve(parentValue, { id }, ctx) {
                return Client.model
                    .aggregate(
                        [
                            {
                                '$match': { '_id': new ObjectId(id) }
                            },
                            {
                                '$lookup': {
                                    'from': User.model.collection.name,
                                    'localField':  'UserIds',
                                    'foreignField': '_id',
                                    'as': 'Users',
                                }
                            },
                            {
                                '$lookup': {
                                    'from': Address.model.collection.name,
                                    'localField':  'AddressIds',
                                    'foreignField': '_id',
                                    'as': 'Addresses',
                                }
                            },
                        ]
                    ).exec().then(client =>{
                    if (client === null || client.length === 0){
                        throw Error("Client not found.");
                    }
                    if (client[0].DateDeleted !== undefined) {
                        throw Error("Client not found.");
                    }

                    return client[0];
                });
            }
        },
        orders: {
            type: OrderList,
            args:{
                page: { type: GraphQLNonNull(GraphQLInt) },
                pageSize: { type: GraphQLNonNull(GraphQLInt) },
                sorted: { type: GraphQLList(sorted) },
                filtered: { type: GraphQLList(filtered) }
            },
            resolve: getOrders
        },
        items: {
            type: ItemList,
            args:{
                page: { type: GraphQLNonNull(GraphQLInt) },
                pageSize: { type: GraphQLNonNull(GraphQLInt) },
                sorted: { type: GraphQLList(sorted) },
                filtered: { type: GraphQLList(filtered) }
            },
            resolve: getItems
        },
        item: {
            type: Item.type,
            args: { id: {type: GraphQLNonNull(GraphQLID)} },
            resolve: getItem
        }
    })
});

module.exports = Query;
