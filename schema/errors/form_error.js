module.exports = class FormError extends Error {
    constructor(message, formError) {
        super(message);
        this.extensions = formError;
        this.nodes = [];
        this.path = [];
    }
};

