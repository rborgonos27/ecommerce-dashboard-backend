const CreateAddressInput = require('./create_address_input');
const CreateClientUserInput = require('./create_client_user_input');

module.exports = {
    CreateAddressInput,
    CreateClientUserInput
};
