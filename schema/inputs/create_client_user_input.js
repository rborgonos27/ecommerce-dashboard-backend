const graphql = require('graphql');
const {
    GraphQLString,
    GraphQLInputObjectType,
    GraphQLNonNull,
} = graphql;
const { UserStatus } = require('../types');
const CreateClientUserInput = new GraphQLInputObjectType({
    name: 'CreateClientUserInput',
    description: 'Input payload for creating client user',
    fields: {
        'Email': { type: GraphQLNonNull(GraphQLString) },
        'FirstName': { type: GraphQLNonNull(GraphQLString) },
        'LastName': { type: GraphQLNonNull(GraphQLString) },
        'Password': { type: GraphQLNonNull(GraphQLString) },
    },
});

module.exports = CreateClientUserInput;
