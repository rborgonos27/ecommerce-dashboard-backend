const graphql = require('graphql');
const {
    GraphQLString,
    GraphQLInputObjectType,
    GraphQLNonNull,
} = graphql;
const CreateAddressInput = new GraphQLInputObjectType({
    name: 'CreateAddressInput',
    description: 'Input payload for creating address',
    fields: {
        'Nickname': { type: GraphQLNonNull(GraphQLString) },
        'ShippingName': { type: GraphQLNonNull(GraphQLString) },
        'Address1': { type: GraphQLNonNull(GraphQLString) },
        'Address2': { type: GraphQLString },
        'City': { type: GraphQLNonNull(GraphQLString) },
        'State': { type: GraphQLNonNull(GraphQLString) },
        'PostalCode': { type: GraphQLNonNull(GraphQLString) },
        'PhoneNumber': { type: GraphQLNonNull(GraphQLString) },
    },
});

module.exports = CreateAddressInput;
