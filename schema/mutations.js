require('dotenv').config();
const graphql = require('graphql');
const { GraphQLObjectType, GraphQLString, GraphQLNonNull, GraphQLID, GraphQLList } = graphql;
const User = require('../models/users');
const Client = require('../models/clients');
const { addUser, updateUser, deleteUser } = require('./resolver/user');
const { clientSetup, updateClient, deleteClient } = require('./resolver/client');
const { UserStatus, UserRoles, AmazonMarketplace, ResponseMessage, UserResponseMessage  } = require('./types');
const { CreateAddressInput } = require('./inputs');

const mutation = new GraphQLObjectType({
    name: 'Mutations',
    fields: {
        AddUser: {
            type: User.type,
            description: 'Add user record.',
            args:{
                FirstName: { type: GraphQLNonNull(GraphQLString) },
                LastName: { type: GraphQLNonNull(GraphQLString) },
                Email: { type: GraphQLNonNull(GraphQLString) },
                Status: { type: GraphQLNonNull(UserStatus) },
                Password: { type: GraphQLNonNull(GraphQLString) },
                ConfirmPassword: { type: GraphQLNonNull(GraphQLString) },
                Role: { type: GraphQLNonNull(UserRoles) },
            },
            resolve: addUser
        },
        UpdateUser: {
            type: User.type,
            description: 'Update user record.',
            args:{
                id: { type: GraphQLNonNull(GraphQLID) },
                FirstName: { type: GraphQLString },
                LastName: { type: GraphQLString },
                Password: { type: GraphQLString },
                ConfirmPassword: { type: GraphQLString },
                Status: { type: UserStatus },
                Role: { type: UserRoles },
            },
            resolve: updateUser
        },
        DeleteUser: {
            type: UserResponseMessage,
            description: 'Delete user.',
            args: { id: { type: GraphQLID } },
            resolve: deleteUser
        },
        SetupClient: {
            type: Client.type,
            description: 'Setup Amazon Seller Client',
            args: {
                Name: { type: GraphQLNonNull(GraphQLString), description: 'Client Name'},
                MarketPlace: { type: GraphQLNonNull(GraphQLList(AmazonMarketplace)), description: 'Marketplace of client'},
                SellerId: {type: GraphQLString, description: 'Amazon Seller Id'},
                MwsAuthToken: {type: GraphQLString, description: 'MWS Auth token'},
                UserIds: {type: GraphQLNonNull(GraphQLList(GraphQLID)), description: 'Users for this client.'},
                Address: {type: GraphQLNonNull(CreateAddressInput), description: 'Client warehouse address'},
            },
            resolve: clientSetup
        },
        UpdateClient: {
            type: Client.type,
            description: 'Update Amazon Seller Client',
            args: {
                id: { type: GraphQLNonNull(GraphQLID), description: 'Client Id' },
                Name: { type: GraphQLString, description: 'Client Name'},
                MarketPlace: { type: GraphQLList(AmazonMarketplace), description: 'Marketplace of client'},
                SellerId: {type: GraphQLString, description: 'Amazon Seller Id'},
                MwsAuthToken: {type: GraphQLString, description: 'MWS Auth token'},
                UserIds: {type: GraphQLList(GraphQLID), description: 'Users for this client.'},
                Address: {type: CreateAddressInput, description: 'Client warehouse address'},
            },
            resolve: updateClient
        },
        DeleteClient: {
            type: ResponseMessage,
            description: 'Delete Client.',
            args: { id: { type: GraphQLID } },
            resolve: deleteClient
        }
    }
});

module.exports = mutation;