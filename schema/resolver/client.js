const { Address, User } = require('../../models');
const Client = require('../../models/clients');
const moment = require('moment');
const { ObjectId } = require('mongoose').Types;

const clientSetup = async (root, args) => {
    const { Name, MarketPlace , SellerId, MwsAuthToken, UserIds  } = args;
    const currentDate = moment().format('YYYY-MM-DD HH:mm:ss');
    const address = args.Address;
    address.DateCreated = currentDate;
    address.DateUpdated = currentDate;
    let addressInsert = new Address.model(address);
    addressInsert = await addressInsert.save();
    const IsSetupComplete = SellerId !== undefined && MwsAuthToken !== undefined;
    const client = {
        Name,
        MarketPlace,
        SellerId,
        MwsAuthToken,
        UserIds,
        IsSetupComplete,
        AddressIds: [addressInsert._id],
        DateCreated: currentDate,
        DateUpdated: currentDate
    };
    const clientObject = await Client.model.findOne({ Name }).exec().then((client) => client);
    if (clientObject) {
        throw new Error("Client name already exists.");
    }
    let clientInsert = new Client.model(client);
    await clientInsert.save();
    return Client.model
        .aggregate(
            [
                {
                    '$match': { '_id': new ObjectId(clientInsert._id) }
                },
                {
                    '$lookup': {
                        'from': User.model.collection.name,
                        'localField':  'UserIds',
                        'foreignField': '_id',
                        'as': 'Users',
                    }
                },
                {
                    '$lookup': {
                        'from': Address.model.collection.name,
                        'localField':  'AddressIds',
                        'foreignField': '_id',
                        'as': 'Addresses',
                    }
                },
            ]
        ).exec().then(client => client[0]);
};

const updateClient = async (root, args) => {
    const { id, Name, MarketPlace , SellerId, MwsAuthToken, UserIds  } = args;
    const clientObject = await Client.model.findById(id).exec().then((client) => client);
    if (!clientObject) {
        throw new Error("Client not found.");
    } else if (clientObject.DateDeleted) {
        throw new Error("Client is already deleted.");
    }
    const currentDate = moment().format('YYYY-MM-DD HH:mm:ss');
    const client = {};
    let addressInsert = {};
    if (args.Address) {
        const address = args.Address;
        address.DateUpdated = currentDate;
        addressInsert = new Address.model(address);
        addressInsert = await addressInsert.save();
        client.AddressIds = [addressInsert._id];
    }

    if (Name) {
        client.Name = Name;
    }
    if (MarketPlace) {
        client.MarketPlace = MarketPlace;
    }
    if (SellerId) {
        client.SellerId = SellerId;
    }
    if (MwsAuthToken) {
        client.MwsAuthToken = MwsAuthToken;
    }
    if (UserIds) {
        client.UserIds = UserIds;
    }
    if (!clientObject.IsSetupComplete) {
        client.IsSetupComplete = !!(clientObject.SellerId && clientObject.MwsAuthToken);
        console.log('isSetupComplete', client.IsSetupComplete);
    }
    if (SellerId && MwsAuthToken) {
        client.IsSetupComplete = true;
    }
    await Client.model.findOneAndUpdate({_id: id}, client, {new: true});
    return Client.model
        .aggregate(
            [
                {
                    '$match': { '_id': new ObjectId(id) }
                },
                {
                    '$lookup': {
                        'from': User.model.collection.name,
                        'localField':  'UserIds',
                        'foreignField': '_id',
                        'as': 'Users',
                    }
                },
                {
                    '$lookup': {
                        'from': Address.model.collection.name,
                        'localField':  'AddressIds',
                        'foreignField': '_id',
                        'as': 'Addresses',
                    }
                },
            ]
        ).exec().then(client => client[0]);
};

const deleteClient = async (root, { id }) => {
    return Client.model.findById(id).exec()
        .then( async client =>{
            if (client === null){
                throw Error("Client not found.");
            }
            if (client.DateDeleted) {
                throw Error("Client is already deleted.");
            }
            const DateDeleted = moment().format('YYYY-MM-DD HH:mm:ss');
            await Client.model.findOneAndUpdate({ _id: id }, { DateDeleted });
            return { Success: true, Message: "Successfully deleted." };
        });
};

module.exports = { clientSetup, updateClient, deleteClient };
