const Order = require('../../models/orders');
const _ = require('lodash');
const getOrders = (root, args) => {
    let sort = {};
    if (args.sorted !== undefined && args.sorted.length)
        sort[args.sorted[0].id] = args.sorted[0].desc ? -1 : 1;
    else
        sort = {LastUpdateDate: -1};

    let filter = {};
    if (args.filtered !== undefined && args.filtered.length){
        _.forEach(args.filtered, filterd => {
            const value = filterd.value;
            if (
                filterd.id === 'FirstName'
                || filterd.id === 'LastName'
                || filterd.id === 'Email'
            ) {
                filter[filterd.id] = { $regex: value + '.*', $options : 'i' };
            }
            else {
                filter[filterd.id] = value;
            }
        });
    }
    return Order.model
        .aggregate(
            [
                {
                    '$lookup':
                        {
                            'from': 'OrderItem',
                            'localField': 'AmazonOrderId',
                            'foreignField': 'AmazonOrderId',
                            'as': 'OrderItem',
                        }
                }
            ]
        )
        .skip(args.page * args.pageSize)
        .limit(args.pageSize)
        .collation({ locale: "en" })
        .sort(sort)
        .exec().then(res =>{
            return Order.model.countDocuments(filter).exec().then(count => {
                return {
                    Total: count,
                    Orders: res,
                    PageSize: args.pageSize
                }
            })
        })
};

module.exports = {
    getOrders
};