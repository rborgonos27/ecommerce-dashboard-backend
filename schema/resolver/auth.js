const User = require('../../models/users');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const ForgotPassword = require('../../models/forgot_password');
const moment = require('moment');
const mailgun = require('mailgun-js');

const login = (root, args) => {
    return User.model.findOne({ Email: args.Email.toLowerCase() } ).exec()
        .then(user => {
            if (user === null){
                throw new Error('Authentication failed. User not found.');
            }
            else{
                if(bcrypt.compareSync(args.Password, user.Password)) {
                    const payload = {
                        email: user.Email
                    };
                    const token = jwt.sign(payload, process.env.JWT_SECRET, {
                        expiresIn: "30d"
                    });

                    user.Token = token;
                    return user;
                } else {
                    throw new Error("Login failed. Invalid credential.");
                }
            }
        });
};

const makeId = (length) => {
    let result           = '';
    const characters       = 'abcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
};

const forgotPassword = (root, args) => {
    return User.model.findOne({ Email: args.Email.toLowerCase() }).exec().then( async user => {
        if (user !== null){
            const request = {
                Key: makeId(20),
                Email: args.Email.toLowerCase(),
                DateCreated: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
                DateExpiration:  moment().utc().add(1,'day').format('YYYY-MM-DD HH:mm:ss')
            };
            return ForgotPassword.model(request).save().then(async result =>{
                console.log(result);
                const link = `${process.env.SITE_URL}/auth/reset-password/${request.Key}`
                let data = {
                    from: 'ECommerce Management <no-reply@ecommercemanagement.com>',
                    to: request.Email,
                    subject: 'Forgot Password Request',
                    html: `<p>Hi ${user.FirstName},
                            </p><p>Here is the link to reset your password<br/>
                            <a href="${link}">${link}</a> .</p><p>Thanks<br/>Ecommerce Management</p>`
                };
                const useMailgun = mailgun({apiKey: process.env.MG_KEY, domain: process.env.MG_DOMAIN});
                await useMailgun.messages().send(data, function (error, body) {
                    console.log(body, result);
                    if (error) {throw new Error(error)}
                });
                return result;
            })
        }
        else{
            throw new Error('Email not found')
        }
    });
};

const resetPassword = (root, args) => {
    if (args.Key === undefined || args.Key == '') {
        throw new Error(`Reset password key can't be blank`)
    }
    if (args.Password === undefined || args.Password.trim() == '') {
        throw new Error(`New password can't be blank`)
    }
    return ForgotPassword.model.findOne({ Key: args.Key }).exec().then(request => {
        if (request === null) { throw new Error(`Reset password key not found`) }

        if (moment().utc() > moment(request.DateExpiration).utc()) {
            throw new Error(`Reset password request has expired`);
        }

        return User.model.findOne({ Email: request.Email }).exec().then(async user => {
            if (user === null) {
                throw new Error(`User not found`);
            }

            user.Password = bcrypt.hashSync(args.Password.trim(), parseInt(process.env.SALT));
            await user.save();
            const payload = {
                email: user.Email
            };
            const token = await jwt.sign(payload, process.env.JWT_SECRET, {
                expiresIn: "30d"
            });

            user.Token = token;
            return user;
        });
    });
};

module.exports = {
    login,
    forgotPassword,
    resetPassword,
};
