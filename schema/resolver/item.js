const Item = require('../../models/items');
const _ = require('lodash');
const getItems = (root, args) => {
    let sort = {};
    if (args.sorted !== undefined && args.sorted.length)
        sort[args.sorted[0].id] = args.sorted[0].desc ? -1 : 1;
    else
        sort = {LastUpdateDate: -1};

    let filter = {};
    if (args.filtered !== undefined && args.filtered.length){
        _.forEach(args.filtered, filterd => {
            const value = filterd.value;
            if (
                filterd.id === 'FirstName'
                || filterd.id === 'LastName'
                || filterd.id === 'Email'
            ) {
                filter[filterd.id] = { $regex: value + '.*', $options : 'i' };
            }
            else {
                filter[filterd.id] = value;
            }
        });
    }
    return Item.model
        .find(filter)
        .skip(args.page * args.pageSize)
        .limit(args.pageSize)
        .collation({ locale: "en" })
        .sort(sort)
        .exec().then(res =>{
            return Item.model.countDocuments(filter).exec().then(count => {
                return {
                    Total: count,
                    Items: res,
                    PageSize: args.pageSize
                }
            })
        })
};

const getItem = (parentValue, { id }) => {
    return Item.model.findById(id).exec().then(item =>{
        if (item === null){
            throw Error("Item not found.");
        }
        return item;
    });
};

module.exports = {
    getItems,
    getItem
};