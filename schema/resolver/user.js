const User = require('../../models/users');
const { isValidPassword, isValidEmail } = require('../../utils/helper');
const FormError = require('../errors/form_error');
const moment = require('moment');
const bcrypt = require('bcrypt');
const _ = require('lodash');

const addUser = (root, args) => {
    const formError = userValidation(args);
    if (Object.keys(formError).length > 0) {
        throw new FormError("Validation error.", { formError })
    }
    delete args.ConfirmPassword;
    return User.model.findOne({ Email: args.Email, DateDeleted: null }).exec()
        .then(user =>{
            if (user === null){
                let currentDate = moment().format('YYYY-MM-DD HH:mm:ss');
                args.DateCreated = currentDate;
                args.DateUpdated = currentDate;
                args.Email = args.Email.toLowerCase();
                args.Password = bcrypt.hashSync(args.Password, parseInt(process.env.SALT));
                let insert = new User.model(args);
                return insert.save();
            }
            else {
                formError.Email = "Email already exists."
                throw new FormError("Create User Error", { formError });
            }
        });
};

const updateUser = (root, args) => {
    const formError = userValidation(args);
    if (Object.keys(formError).length > 0) {
        throw new FormError("Validation error.", { formError })
    }
    delete args.ConfirmPassword;
    return User.model.findOne({ _id: args.id, DateDeleted: null }).exec()
        .then(user => {
            if (user === null) {
                throw new Error("User not found.");
            }
            args.DateUpdated = moment().format('YYYY-MM-DD HH:mm:ss');
            if (args.Password) {
                args.Password = bcrypt.hashSync(args.Password, parseInt(process.env.SALT));
            }
            return User.model.findOneAndUpdate({_id: args.id}, args, {new: true});
        });
};

const deleteUser = async (root, { id }) => {
    return User.model.findOne({ _id: id, DateDeleted: null }).exec()
        .then( async user =>{
            if (user === null){
                throw new Error("User not found.");
            }
            const DateDeleted = moment().format('YYYY-MM-DD HH:mm:ss');
            await User.model.findOneAndUpdate({ _id: id }, { DateDeleted });
            return {
                Success: true,
                Message: "Successfully deleted.",
                User: user
            };
        });
};

const userValidation = (args) => {
    const formError = {};
    if (args.Password !== undefined) {
        if (args.Password.length === 0) {
            formError.Password = "Password is empty.";
        }
        if (args.ConfirmPassword !== args.Password) {
            formError.Password = "Password not matched.";
            formError.ConfirmPassword = "Password not matched.";
        }
    }
    if (args.Password && !isValidPassword(args.Password)) {
        formError.Password = "Password must be eight characters," +
            " at least one uppercase letter, one lowercase letter," +
            " one number and one special character.";
    }
    if (args.Email && !isValidEmail(args.Email)) {
        formError.Email = "Invalid email format.";
    }
    return formError;
};

const getUser = (parentValue, { id }) => {
    return User.model.findById(id).exec().then(user =>{
        if (user === null || user.DateDeleted !== undefined){
            throw Error("User not found.");
        }
        return user;
    });
};

const getUsers = (parentValue, args) => {
    let sort = {};
    if (args.sorted !== undefined && args.sorted.length)
        sort[args.sorted[0].id] = args.sorted[0].desc ? -1 : 1;
    else
        sort = {DateCreated: -1};

    let filter = { DateDeleted: null };
    if (args.filtered !== undefined && args.filtered.length){
        _.forEach(args.filtered, filterd => {
            const value = filterd.value;
            if (
                filterd.id === 'FirstName'
                || filterd.id === 'LastName'
                || filterd.id === 'Email'
            ) {
                filter[filterd.id] = { $regex: value + '.*', $options : 'i' };
            }
            else {
                filter[filterd.id] = value;
            }
        });
    }
    return User.model
        .find(filter)
        .skip(args.page * args.pageSize)
        .limit(args.pageSize)
        .collation({ locale: "en" })
        .sort(sort)
        .exec().then(res =>{
            return User.model.countDocuments(filter).exec().then(count => {
                return {
                    Total: count,
                    Users: res,
                    PageSize: args.pageSize
                }
            })
        })
};

module.exports = {
    addUser,
    updateUser,
    deleteUser,
    getUser,
    getUsers,
};
