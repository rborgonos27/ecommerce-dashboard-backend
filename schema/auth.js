require('dotenv').config();
const graphql = require('graphql');
const { GraphQLSchema, GraphQLObjectType, GraphQLNonNull, GraphQLString } = graphql;
const { UserAuth } = require('./types');
const ForgotPassword = require('../models/forgot_password');
const { login, forgotPassword, resetPassword } = require('./resolver/auth');

module.exports = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: "Query",
        fields: {
            LoginInfo: {
                type: new GraphQLObjectType({
                    name: 'UserInfo',
                    fields: {
                        'Email': {type: GraphQLString},
                        'Password': {type: GraphQLString},
                    }
                }),
                resolve: () => {
                    return {
                        Email: "Email parameter",
                        Password: "Secure password",
                    }
                }
            }
        }
    }),
    mutation: new GraphQLObjectType({
        name: 'Mutations',
        fields: {
            Login: {
                type: UserAuth,
                description: "Login user",
                args: {
                    Email: {type: GraphQLNonNull(GraphQLString)},
                    Password: {type: GraphQLNonNull(GraphQLString)},
                },
                resolve: login
            },
            ForgotPassword: {
                type: ForgotPassword.type,
                description: "Forgot password feature for user",
                args: {
                    Email: {type: GraphQLNonNull(GraphQLString)},
                },
                resolve: forgotPassword
            },
            ResetPassword: {
                type: UserAuth,
                description: "Reset Password Feature",
                args: {
                    Password: {type: GraphQLNonNull(GraphQLString)},
                    ConfirmPassword: {type: GraphQLNonNull(GraphQLString)},
                    Key: {type: GraphQLNonNull(GraphQLString)},
                },
                resolve: resetPassword
            }
        }
    })
});
