const graphql = require('graphql');
const User = require('../../models/users');
const { GraphQLObjectType, GraphQLList, GraphQLInt } = graphql;

const UserList = new GraphQLObjectType({
    name: 'UserList',
    fields: {
        'PageSize': {type: GraphQLInt },
        'Total': {type: GraphQLInt },
        'Users': {type: GraphQLList(User.type)},
    }
});

module.exports = UserList;
