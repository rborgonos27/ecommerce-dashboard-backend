const graphql = require('graphql');
const { GraphQLString, GraphQLObjectType } = graphql;

const UserAuth = new GraphQLObjectType({
    name: 'UserAuth',
    fields: {
        'id': {type: GraphQLString, resolve: ({ _id }) => _id.toString()},
        'Email': {type: GraphQLString},
        'FirstName': {type: GraphQLString},
        'LastName': {type: GraphQLString},
        'Token': {type: GraphQLString},
        'Role': {type: GraphQLString},
    }
});

module.exports = UserAuth;
