const graphql = require('graphql');
const { GraphQLObjectType, GraphQLString } = graphql;

const SalesRank = new GraphQLObjectType({
    name: 'SalesRank',
    fields: {
        'ProductCategoryId': {type: GraphQLString },
        'Rank': {type: GraphQLString }
    }
});

module.exports = SalesRank;
