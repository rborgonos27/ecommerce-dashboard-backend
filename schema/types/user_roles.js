const graphql = require('graphql');
const { GraphQLEnumType } = graphql;

const UserRoles = new GraphQLEnumType({
    name: 'UserRoles',
    values: {
        SuperAdmin: { value: 'SuperAdmin', description: 'Super Admin' },
        Manager: { value: 'Manager', description: 'Manager'  },
        Admin: { value: 'Admin', description: 'Admin'  },
        Viewer: { value: 'Viewer', description: 'Viewer'  },
    }
});

module.exports = UserRoles;
