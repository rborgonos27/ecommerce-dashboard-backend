const graphql = require('graphql');
const { GraphQLEnumType } = graphql;

const UserStatus = new GraphQLEnumType({
    name: 'UserStatus',
    values: {
        Active: { value: 'Active', description: 'Active' },
        Inactive: { value: 'Inactive', description: 'Inactive' },
    }
});

module.exports = UserStatus;
