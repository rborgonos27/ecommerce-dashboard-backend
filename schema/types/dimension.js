const graphql = require('graphql');
const { GraphQLObjectType, GraphQLString } = graphql;
const ValueUnit = require('./value_unit');

const Dimension = new GraphQLObjectType({
    name: 'Dimension',
    fields: {
        'Height': { type: ValueUnit('Height') },
        'Weight': { type: ValueUnit('Weight') },
        'Width': { type: ValueUnit('Width') },
        'Length': { type: ValueUnit('Length') },
    }
});

module.exports = Dimension;
