const graphql = require('graphql');
const Order = require('../../models/orders');
const { GraphQLObjectType, GraphQLList, GraphQLInt } = graphql;

const OrderList = new GraphQLObjectType({
    name: 'OrderList',
    fields: {
        'PageSize': {type: GraphQLInt },
        'Total': {type: GraphQLInt },
        'Orders': {type: GraphQLList(Order.type)}
    }
});

module.exports = OrderList;
