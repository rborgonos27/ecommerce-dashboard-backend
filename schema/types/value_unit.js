const graphql = require('graphql');
const { GraphQLObjectType, GraphQLString } = graphql;

const ValueUnit = (name) => {
    return new GraphQLObjectType({
        name,
        fields: {
            'Value': {type: GraphQLString },
            'Units': {type: GraphQLString }
        }
    });
};

module.exports = ValueUnit;

