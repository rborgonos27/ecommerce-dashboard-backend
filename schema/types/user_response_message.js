const graphql = require('graphql');
const { GraphQLString, GraphQLObjectType, GraphQLBoolean } = graphql;
const user = require('../../models/users');

const UserResponseMessage = new GraphQLObjectType({
    name: 'UserResponseMessage',
    fields: {
        'Success': {type: GraphQLBoolean},
        'Message': {type: GraphQLString},
        'User': { type: user.type }
    }
});

module.exports = UserResponseMessage;
