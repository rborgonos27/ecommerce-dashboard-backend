const graphql = require('graphql');
const Item = require('../../models/items');
const { GraphQLObjectType, GraphQLList, GraphQLInt } = graphql;

const ItemList = new GraphQLObjectType({
    name: 'ItemList',
    fields: {
        'PageSize': {type: GraphQLInt },
        'Total': {type: GraphQLInt },
        'Items': {type: GraphQLList(Item.type)}
    }
});

module.exports = ItemList;
