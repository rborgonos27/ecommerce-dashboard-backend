const graphql = require('graphql');
const { GraphQLInputObjectType, GraphQLNonNull, GraphQLBoolean, GraphQLString  } = graphql;

const sorted = new GraphQLInputObjectType({
    name: "Sorted",
    fields: {
        id: {type: GraphQLNonNull(GraphQLString)},
        desc: { type: GraphQLNonNull(GraphQLBoolean)}
    }
});

module.exports = sorted;
