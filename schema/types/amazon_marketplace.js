const graphql = require('graphql');
const { GraphQLEnumType } = graphql;

const AmazonMarketplace = new GraphQLEnumType({
    name: 'AmazonMarketplace',
    values: {
        Brazil: {
            value: {
                CountryCode: 'BR',
                EndPoint: 'https://mws.amazonservices.com',
                MarketPlaceId: 'A2Q3Y263D00KWC',
            },
            description: 'Marketplace for Brazil',
        },
        Canada: {
            value: {
                CountryCode: 'CA',
                EndPoint: 'https://mws.amazonservices.ca',
                MarketPlaceId: 'A2Q3Y263D00KWC',
            },
            description: 'Marketplace for Canada',
        },
        Mexico: {
            value: {
                CountryCode: 'MX',
                EndPoint: 'https://mws.amazonservices.com.mx',
                MarketPlaceId: 'A1AM78C64UM0Y8',
            },
            description: 'Marketplace for Mexico',
        },
        US: {
            value: {
                CountryCode: 'US',
                EndPoint: 'https://mws.amazonservices.com',
                MarketPlaceId: 'ATVPDKIKX0DER',
            },
            description: 'Marketplace for US',
        },
        UAE: {
            value: {
                CountryCode: 'AE',
                EndPoint: 'https://mws.amazonservices.ae',
                MarketPlaceId: 'A2VIGQ35RCS4UG',
            },
            description: 'Marketplace for UAE',
        },
        Germany: {
            value: {
                CountryCode: 'DE',
                EndPoint: 'https://mws-eu.amazonservices.com',
                MarketPlaceId: 'A1PA6795UKMFR9',
            },
            description: 'Marketplace for Germany',
        },
        Egypt: {
            value: {
                CountryCode: 'EG',
                EndPoint: 'https://mws-eu.amazonservices.com',
                MarketPlaceId: 'ARBP9OOSHTCHU',
            },
            description: 'Marketplace for Egypt',
        },
        Spain: {
            value: {
                CountryCode: 'ES',
                EndPoint: 'https://mws-eu.amazonservices.com',
                MarketPlaceId: 'A1RKKUPIHCS9HS',
            },
            description: 'Marketplace for Spain',
        },
        France: {
            value: {
                CountryCode: 'FR',
                EndPoint: 'https://mws-eu.amazonservices.com',
                MarketPlaceId: 'A1RKKUPIHCS9HS',
            },
            description: 'Marketplace for France',
        },
        UK: {
            value: {
                CountryCode: 'GB',
                EndPoint: 'https://mws-eu.amazonservices.com',
                MarketPlaceId: 'A1F83G8C2ARO7P',
            },
            description: 'Marketplace for UK',
        },
        India: {
            value: {
                CountryCode: 'IN',
                EndPoint: 'https://mws.amazonservices.in',
                MarketPlaceId: 'A21TJRUUN4KGV',
            },
            description: 'Marketplace for India',
        },
        Italy: {
            value: {
                CountryCode: 'IT',
                EndPoint: 'https://mws-eu.amazonservices.com',
                MarketPlaceId: 'APJ6JRA9NG5V4',
            },
            description: 'Marketplace for Italy',
        },
        Netherlands: {
            value: {
                CountryCode: 'NL',
                EndPoint: 'https://mws-eu.amazonservices.com',
                MarketPlaceId: 'A1805IZSGTT6HS',
            },
            description: 'Marketplace for Netherlands',
        },
        SaudiArabia: {
            value: {
                CountryCode: 'SA',
                EndPoint: 'https://mws-eu.amazonservices.com',
                MarketPlaceId: 'A17E79C6D8DWNP',
            },
            description: 'Marketplace for Saudi Arabia',
        },
        Turkey: {
            value: {
                CountryCode: 'TR',
                EndPoint: 'https://mws-eu.amazonservices.com',
                MarketPlaceId: 'A33AVAJ2PDY3EV',
            },
            description: 'Marketplace for Turkey',
        },
        Singapore: {
            value: {
                CountryCode: 'SG',
                EndPoint: 'https://mws-fe.amazonservices.com',
                MarketPlaceId: 'A19VAU5U5O7RUS',
            },
            description: 'Marketplace for Singapore',
        },
        Australia: {
            value: {
                CountryCode: 'AU',
                EndPoint: 'https://mws.amazonservices.com.au',
                MarketPlaceId: 'A39IBJ37TRP1C6',
            },
            description: 'Marketplace for Australia',
        },
        Japan: {
            value: {
                CountryCode: 'JP',
                EndPoint: 'https://mws.amazonservices.jp',
                MarketPlaceId: 'A1VC38T7YXB528',
            },
            description: 'Marketplace for Japan',
        },
    }
});

module.exports = AmazonMarketplace;
