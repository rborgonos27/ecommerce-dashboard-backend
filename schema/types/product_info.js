const graphql = require('graphql');
const { GraphQLObjectType, GraphQLInt } = graphql;

const ProductInfo = new GraphQLObjectType({
    name: 'ProductInfo',
    fields: {
        'NumberOfItems': { type: GraphQLInt },
    }
});

module.exports = ProductInfo;
