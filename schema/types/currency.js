const graphql = require('graphql');
const { GraphQLObjectType, GraphQLFloat, GraphQLString } = graphql;

const Currency = new GraphQLObjectType({
    name: 'Currency',
    fields: {
        'Amount': { type: GraphQLFloat },
        'CurrencyCode': { type: GraphQLString },
    }
});

module.exports = Currency;
