const graphql = require('graphql');
const { GraphQLString, GraphQLObjectType, GraphQLBoolean } = graphql;

const ResponseMessage = new GraphQLObjectType({
    name: 'ResponseMessage',
    fields: {
        'Success': {type: GraphQLBoolean},
        'Message': {type: GraphQLString},
    }
});

module.exports = ResponseMessage;
