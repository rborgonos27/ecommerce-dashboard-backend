const graphql = require('graphql');
const { GraphQLObjectType, GraphQLString } = graphql;
const ValueUnit = require('./value_unit');

const Image = new GraphQLObjectType({
    name: 'Image',
    fields: {
        'URL': { type: GraphQLString },
        'Height': { type: ValueUnit('ImageHeight') },
        'Width': { type: ValueUnit('ImageWidth') },
    }
});

module.exports = Image;
