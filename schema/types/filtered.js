const graphql = require('graphql');
const { GraphQLInputObjectType, GraphQLNonNull, GraphQLString  } = graphql;

const filtered = new GraphQLInputObjectType({
    name: "Filtered",
    fields: {
        id: {type: GraphQLNonNull(GraphQLString)},
        value: { type: GraphQLNonNull(GraphQLString)}
    }
});

module.exports = filtered;
