const graphql = require('graphql');
const Client = require('../../models/clients');
const { GraphQLObjectType, GraphQLList, GraphQLInt } = graphql;

const ClientList = new GraphQLObjectType({
    name: 'ClientList',
    fields: {
        'PageSize': {type: GraphQLInt },
        'Total': {type: GraphQLInt },
        'Clients': {type: GraphQLList(Client.type)}
    }
});

module.exports = ClientList;
