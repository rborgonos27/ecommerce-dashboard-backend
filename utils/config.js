require('dotenv').config();
let whitelist = process.env.WHITELIST_URLS;
whitelist = whitelist.split(',');
const corsOptions = function (req, callback) {
    if (!req.headers.host || whitelist.indexOf(req.headers.host) !== -1) {
        callback(null, true)
    } else {
        callback(new Error('Not allowed by CORS'))
    }
};

module.exports = {
    'anonymousURL' : [ '/', '/auth/iql', '/auth/iql?','/auth/graphql', '/iql', '/iql?', '/favicon.ico'],
    corsOptions,
};