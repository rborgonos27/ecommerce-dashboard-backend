const User = require('../models/users');

module.exports = {
    getUser: async email => {
        return User.model.findOne({'Email' : email}).exec().then( results =>{
            if (!results) {
                throw new Error("User not found.");
            }
            return results;
        });
    }
};