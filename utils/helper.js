const isValidPassword = (password) => {
    if (password.length < 8) return false;
    let hasBigLetter   = false;
    let hasSmallLetter = false;
    let hasNumber      = false;
    let hasSpecialCharacter = false;
    for (let i = 0; i < password.length; i++) {
        const charCode = password.charCodeAt(i);
        if(charCode > 47 && charCode < 58) {
            hasNumber = true;
        }
        if(charCode > 64 && charCode < 91) {
            hasBigLetter = true;
        }
        if(charCode > 96 && charCode < 123) {
            hasSmallLetter = true;
        }
        if(charCode > 32 && charCode < 48) {
            hasSpecialCharacter = true;
        }
        if(charCode > 57 && charCode < 65) {
            hasSpecialCharacter = true;
        }
        if(charCode > 90 && charCode < 97) {
            hasSpecialCharacter = true;
        }

    }
    return hasBigLetter && hasSmallLetter && hasNumber && hasSpecialCharacter;
};

const isValidEmail = (email) => {
    const atSymbol = email.indexOf("@");
    if(atSymbol < 1) return false;
    const domain = email.substring(email.indexOf('@') + 1, email.length);
    const dot = domain.indexOf(".");
    return dot >= 1;

};

const isEmptyObject = (object) => !Object.keys(object).length;

module.exports = {
    isValidPassword,
    isValidEmail,
    isEmptyObject,
};
