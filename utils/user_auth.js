const appUser = require('./app_user');
const config = require('./config');
const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    let redirectUrl = req.originalUrl;

    if(config.anonymousURL.includes(redirectUrl)) {
        next();
    }
    else {
        let token = req.body.token || req.query.token || req.headers['x-access-token'];
        if (token) {
            new Promise((resolve, reject) => {
                return jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
                    if (err) {
                        reject('Failed to authenticate user token.');
                    } else {
                        resolve(decoded);
                    }
                });
            }).then( data =>{
                req.decoded = data;
                appUser.getUser(data.Email).then(u =>{
                    req.decoded.user = u;
                    next();
                });
            })
                .catch(err =>{
                    res.json({ success: false, message: err })
                });

        } else {
            next();
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });
        }
    }
};
