require('dotenv').config();
const { GraphQLID, GraphQLString } = require("graphql");
const GraphQLDate = require('graphql-date');
const _ = require('lodash');

const Columns = {
    GenerateGraphQlSchema: (cols, valSuffix = false, exclude = []) => {
        let fields = {id: { type: GraphQLID, resolve: ({ _id }) => _id ? _id.toString() : null }};
        for(const key in cols){
            fields[key] = {type: cols[key].graphQL ? cols[key].graphQL : GraphQLString};
            if (valSuffix){
                if (cols[key].graphQL !== undefined){
                    fields[`${key}_val`] = {type: cols[key].graphQL}
                } else {
                    fields[`${key}_val`] = {type: GraphQLString}
                }
            }
        }
        return _.omit(fields, exclude);
    },
    GenerateSchema: (cols, valSuffix = false, exclude = []) => {
        const schema = {};
        for(const key in cols){
            schema[key] = cols[key].type;
            if (valSuffix) {
                schema[`${key}_val`] = cols[key].type;
            }
        }
        return _.omit(schema, exclude);
    }
};

module.exports = Columns;
