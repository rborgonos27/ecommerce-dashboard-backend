const cron = require('node-cron');
const { getOrderDetails, getOrders, getProductDetails } = require('../mws');

// every minute
cron.schedule('* * * * *', () => {
    console.log('run every minute');
});

// every fifteen minutes
cron.schedule('*/15 * * * *', () => {
    // get order first
    getOrders();
});

// every twenty minues
cron.schedule('*/20 * * * *', () => {
    // proposed to use queuing system to fetch order details
    getOrderDetails();
    console.log('run every fifteen minutes');
});

// every two hours
cron.schedule('0 */2 * * *', () => {
    console.log('run every two hours');
});

