require('dotenv').config();
const express = require('express');
const cors = require('cors');
const router = express.Router();
const bodyParser = require('body-parser');
const graphqlRoute = require('./routes/graphql');
const authGraphqlRoute = require('./routes/auth');
const { config } = require('./utils');
const app = express();
const port = process.env.PORT || 5000;

// load db
require('./db');
let whitelist = process.env.WHITELIST_URLS || 'http://localhost:2000';
whitelist = whitelist.split(',');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors(config.corsOptions));

// router.use(userAuth);

app.use('/', router);
app.get('/', (req, res) => {
   return res.json({ 'name': 'ecommerse-management-api', 'version': '1' });
});

app.use('/', graphqlRoute);
app.use('/auth', authGraphqlRoute);

require('./cron');
require('./mws').getReportData();
app.listen(port, () => console.log(`Listening on port ${port}`));
