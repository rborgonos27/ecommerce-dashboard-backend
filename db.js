require('dotenv').config();
const mongoose = require('mongoose');
mongoose.connect(`${process.env.CONNECTION_URL}`,{useNewUrlParser: true, useUnifiedTopology: true, dbName: process.env.DATABASE});
const db = mongoose.connection;
db.on('success', con => { console.log('connected')});
db.on('error', err => {
    console.log('MongoDB connection error:');
    console.log(err);
    process.exit(0);
});
