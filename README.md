# Ecommerce Management Dashboard Backend

## Development Setup
* Run `npm install`
* Copy .env.example e.g. `cp .env.example .env`
* Update .env 
`
PORT=<development port>
CONNECTION_URL=<mongodb connection url>
DATABASE=<db name>
JWT_SECRET=<jwt secret>
SALT=<hash salt>
`
* To run type `npm run dev`
* To run unit tests type `npm test`